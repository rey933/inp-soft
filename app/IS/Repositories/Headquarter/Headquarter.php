<?php

namespace App\IS\Repositories\Headquarter;

use App\IS\Repositories\Base\BaseModel;
use Illuminate\Database\Eloquent\Model;

class Headquarter extends BaseModel
{
    protected $table= 'headquarters';
    protected $fillable= [
        'business_id', 'name', 'description', 'direction',
    ];

    public function business()
    {
    	return $this->belongsTo('App\IS\Repositories\Businesses\Business', 'business_id');
    }

    public function blocks()
    {
    	return $this->hasMany('App\IS\Repositories\Blocks\Block', 'headquarter_id');
    }

    public function findings()
    {
        return $this->hasMany('App\IS\Repositories\Reports\Finding', 'business_id');
    }
}
