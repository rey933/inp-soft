@extends('layouts.master')

	@section('content_title')
		Creación de sucursal para la Empresa: {!! $business->name !!}
	@stop

	@section('content')
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		{!! Form::open(['method'=> 'POST', 'id'=> 'create_form', 'action'=> ['Headquarters\HeadquarterController@store', 'business_id'=> $business->id],
						'onsubmit'=> 'return false']) !!}
			<div class="form-group">
				{!! Form::label('headquarter_name', 'Nombre de la Sucursal') !!}
				{!! Form::text('headquarter_name', NULL, ['class'=> 'form-control required',
													   'placeholder'=> 'Ejemplo: Plumrose Latinoamericana - Sede Cagua']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('headquarter_description', 'Descripción de la Sucursal') !!}
				{!! Form::textarea('headquarter_description', NULL, ['class'=> 'form-control required',
													   'placeholder'=> 'Ejemplo: Sede principal donde son tomadas decisiones gerenciales',
													   'rows'=> 3]) !!}
			</div>
			<div class="form-group">
				{!! Form::label('headquarter_direction', 'Dirección de la Sucursal') !!}
				{!! Form::textarea('headquarter_direction', NULL, ['class'=> 'form-control required',
													   'placeholder'=> 'Ejemplo: Av. Fuerzas Armadas, Edif. Libertador',
													   'rows'=> 3]) !!}
			</div>
			<div class="form-group">
				<a href="{!! action('Headquarters\HeadquarterController@index', ['id'=> $business->id]) !!}" class="btn btn-default btn-sm">Volver</a>
				{!! Form::button('Crear Sucursal', ['id'=> 'create_btn', 'class'=> 'btn btn-success btn-sm']) !!}
			</div>
		{!! Form::close() !!}
	@stop

	@section('post_script')
		{!! Html::script('js/jquery.steps.js') !!}
		{!! Html::script('js/localization/messages_es.js') !!}

		<script>
			$(document).ready(function() {
				var form= $('#create_form');
				$('#create_btn').click(function() {
					form.removeAttr('onsubmit');
					form.validate();
					if(form.valid()) {
						form.submit();
					} else{
						return form.valid();
					}
				});
			});
		</script>
	@stop