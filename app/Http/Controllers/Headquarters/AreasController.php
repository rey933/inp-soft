<?php

namespace App\Http\Controllers\Headquarters;

use App\IS\Repositories\Areas\AreaRepo;
use App\IS\Repositories\Departments\DepartmentRepo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AreasController extends Controller
{
    protected $area;
    protected $department;

    public function __construct(AreaRepo $area, DepartmentRepo $department)
    {
        $this->area= $area;
        $this->department= $department;
    }

    public function all()
    {
    	$areas= $this->area->all();
    	return $areas;
    }

    /**
     * Search Areas for the given $department_id
     */
    public function searchByDepartment($department_id)
    {
    	$department= $this->department->search($department_id);
        $areas= $department->areas()->get()->toJson();
        return $areas;
    }

    public function store(Request $request)
    {
    	$data= $this->area->create([
            'name'=> $request->area_name,
            'description'=> $request->area_description,
        ]);

        return response()->json([
            'area'=> [
                'id'=> $data->id,
                'name'=> $data->name,
                'description'=> $data->description,
            ],
        ]);
    }

    public function attachArea($department_id, $area_id)
    {
    	$department= $this->department->search($department_id);
    	$area= $this->area->search($area_id);
    	$department->areas()->attach($area->id);
    	return response()->json([
            'area'=> [
                'id'=> $area->id,
                'name'=> $area->name,
                'description'=> $area->description,
            ],
        ]);
    }

    public function delete($area_id)
    {
    	//First, remove all relations with departments
    	$area= $this->area->search($area_id);
    	$area->departments()->detach();
        $area->delete();
        return response()->json([
            'status'=> 'completed'
        ]);
    }

    public function detachArea($department_id, $area_id)
    {
    	$area= $this->area->search($area_id);
    	$area->departments()->detach($department_id);
    	return response()->json([
            'status'=> 'completed'
        ]);
    }
}
