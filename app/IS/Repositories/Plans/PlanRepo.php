<?php
/**
 * Created by Carlos José Rey Sayago.
 * User: crey
 * Date: 15/09/2016
 * Time: 07:39 AM
 */

namespace App\IS\Repositories\Plans;


use App\IS\Repositories\Base\BaseRepo;

class PlanRepo extends BaseRepo
{

    public function getModel()
    {
        return new Plan();
    }

    public function status()
    {
    	$status= $this->getModel()->status;
    	return $status;
    }
}