<?php

namespace App\IS\Repositories\Reports;

use App\IS\Repositories\Base\BaseModel;

class Accident extends BaseModel
{
    protected $table= 'accidents';
    protected $fillable= [
        'reporter',
        'affected_identity',
        'accident_date',
        'accident_hour',
        'accident_classification',
        'accident_category',
        'business_id',
        'headquarter_id',
        'block_id',
        'department_id',
        'area_id',
        'place_id',
        'place_type_id',
        'specific_place_type',
        'physical_activity_type_id',
        'specific_activity_type',
        'job_type_id',
        'specific_job_type',
        'material_agent_id',
        'specific_material_agent',
        'accident_id',
        'specific_accident',
        'body_part_id',
        'specific_body_part',
        'injure_id',
        'specific_injure',
        'severity',
        'repose_days',
    ];
}
