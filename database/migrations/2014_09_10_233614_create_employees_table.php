<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {            
            $table->string('last_name');
            $table->string('name');            
            //$table->string('identity')->unique(); //xxxxxxxx //Talk with Andreliz about this issue
            $table->string('identity');
            $table->primary('identity');
            $table->date('birth_date'); // YYYY/MM/DD
            $table->date('admission_date');// YYYY/MM/DD
            $table->string('position');
            $table->string('supervisor_identity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
