@extends('layouts.master')
    @if($businesses->count() > 0)
    @section('post_style')
        <style>
            .selection-screen {

            }
                .selection-screen article {
                    height: 200px;
                    background-size: 100%;
                    cursor: pointer;
                    transition: all .5s ease;
                }
                    .selection-screen article:hover {
                        opacity: .7;
                    }
                .selection-screen .injuries-option {
                    background: no-repeat url("{!! asset('img/content-images/findings/injuries-option.jpg') !!}");
                }
                .selection-screen .findings-option {
                    background: no-repeat url("{!! asset('img/content-images/findings/findings.png') !!}");
                }

                .option-content {

                }
                    .option-content .business-selection {

                    }
                        .option-content .business-selection .business {
                            cursor: pointer;
                            transition: all .5s ease;
                        }
                        .option-content .business-selection .business:hover {
                            opacity: .6;
                        }
                            .option-content .business-selection .business img{
                                width: 175px;
                            }
        </style>
        {!! Html::style('css/date/bootstrap-datepicker.css') !!}
    @stop

    @section('content_title')
        Estadísticas
    @stop

    @section('bread')
        <li><a href="{!! url('system/dashboard') !!}">Dashboard</a></li>
        <li><a href="{!! url('system/businesses/') !!}">Empresas</a></li>
        <li class="active">Estadísticas</li>
    @stop

    @section('content')

        <is-statistics :businesses_list.sync="businesses"
                       :headquarters_list.sync="headquarters"></is-statistics>


        <template id="is-statistics-template">
            <!-- Selection Screen -->
            <section class="selection-screen">
                <article class="injuries-option col-md-5 col-xs-12 img-rounded"
                         @click="displayInjuriesSection(false)">
                </article> <!-- /.injuries-option-->
                <article class="findings-option col-md-5 col-md-offset-2 col-xs-12 img-rounded"
                         @click="displayFindingsSection()">
                </article> <!-- /.findings-option-->
            </section> <!-- /#selection-screen-->

            <section id="injuries"
                     style="display: none;">
                <article class="option-content">
                    <article class="business-selection">
                        <a href="#" @click="displaySelectionScreen()"><- Volver al inicio</a>
                        <br>
                        <br>
                        <h4>Seleccione la empresa a consultar:</h4>
                        <hr>
                        <!--Businesses images-->
                        <div class="business col-md-2"
                             v-for="business in businesses_list"
                             @click="displaySearchInjuriesForm(business.id)">
                            <img :src="businessLogoFolder + business.image"
                                 alt="Logo Empresa"
                                 class="img-thumbnail"
                                 :title="business.name">
                        </div>
                    </article> <!-- /.business-selection-->

                    <article class="injuries-form" style="display: none;">
                        <div>
                            <a href="#" @click="displayBusinessScreen()"><- Volver al selector de empresa</a>
                            <br>
                            <br>
                            {!! Form::open(['id'=> 'injuries-form', 'onsubmit'=> 'return false']) !!}
                                <div class="form-group">
                                    {!! Form::label('headquarter_list', 'Sucursal') !!}
                                    <select name="headquarter_id" id="headquarters-list" class="form-control">
                                        <option v-for="headquarter in headquarters_list" value="@{{ headquarter.id  }}">@{{ headquarter.name }}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('graphic_type', 'Seleccione la estadística:') !!}
                                    <select name="graphic_type" id="graphic-type" class="form-control">
                                        <option value="1">Accidentalidad CPT/SPT</option>
                                        <option value="2">Distribución por agente material</option>
                                        <option value="3">Distribución por tipo de accidente</option>
                                        <option value="4">Distribución por parte del cuerpo lesionada</option>
                                        <option value="5">Distribución por naturaleza de la lesión</option>
                                        <option value="6">Informe</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('date_range', 'Seleccione el rango de fecha') !!}
                                    <div class="input-daterange input-group" id="datepicker">
                                        <input type="text" class="input-sm form-control" name="start_date" readonly/>
                                        <span class="input-group-addon">Hasta</span>
                                        <input type="text" class="input-sm form-control" name="end_date" readonly/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-sm btn-default" @click="getFindingsStatistics()">Generar Gráfico</button>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </article> <!-- /.business-form-->
                </article> <!-- /.option-content-->
            </section> <!-- /#injuries-->

            <section id="findings"
                     style="display: none;">
                <article class="option-content">
                    <article class="business-selection">
                        <a href="#" @click="displaySelectionScreen()"><- Volver al inicio</a>
                        <br>
                        <br>
                        <h4>Seleccione la empresa a consultar:</h4>
                        <hr>
                        <!--Businesses images-->
                        <div class="business col-md-2"
                             v-for="business in businesses_list"
                        @click="displaySearchFindingsForm(business.id)">
                        <img :src="businessLogoFolder + business.image"
                             alt="Logo Empresa"
                             class="img-thumbnail"
                             :title="business.name">
                        </div>
                    </article> <!-- /.business-selection-->

                    <article class="findings-form" style="display: none;">
                        <div>
                            <a href="#" @click="displayBusinessScreen()"><- Volver al selector de empresa</a>
                            <br>
                            <br>
                            {!! Form::open(['id'=> 'findings-form', 'onsubmit'=> 'return false']) !!}
                            <div class="form-group">
                                {!! Form::label('headquarter_list', 'Sucursal') !!}
                                <select name="headquarter_id" id="headquarters-list" class="form-control">
                                    <option v-for="headquarter in headquarters_list" value="@{{ headquarter.id  }}">@{{ headquarter.name }}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                {!! Form::label('graphic_type', 'Seleccione la estadística:') !!}
                                <select name="graphic_type" id="graphic-type" class="form-control">
                                    <option value="1">Detectados / Corregidos</option>
                                    <option value="2">% Corrección</option>
                                    <option value="3">Distribución por Estatus</option>
                                    <option value="4">Distribución por nivel de riesgo</option>
                                    <option value="5">Distribución por fuente de hallazgo</option>
                                    <option value="6">Reporte por Persona</option>
                                    <option value="7">Distribución de Hallazgo por Área</option>
                                    <option value="8">Hallazgos Pendientes y En Proceso - Área Resolutoria</option>
                                </select>
                            </div>
                            <div class="form-group">
                                {!! Form::label('date_range', 'Seleccione el rango de fecha') !!}
                                <div class="input-daterange input-group" id="datepicker">
                                    <input type="text" class="input-sm form-control" name="start_date" readonly/>
                                    <span class="input-group-addon">Hasta</span>
                                    <input type="text" class="input-sm form-control" name="end_date" readonly/>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-sm btn-default" @click="getFindingsStatistics()">Generar Gráfico</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </article> <!-- /.business-form-->
                </article> <!-- /.option-content-->
            </section> <!-- /#findings-->
        </template> <!-- /#is-statistics-template-->

        <!--Graph Modal-->
        <div id="graph-modal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gráfico</h4>
                    </div>
                    <div class="modal-body">
                        <canvas id="graph1"></canvas>
                        <section id="graph2"></section>
                        <canvas id="graph3"></canvas>
                        <canvas id="graph4"></canvas>
                        <canvas id="graph5"></canvas>
                        <canvas id="graph6"></canvas>
                        <canvas id="graph7"></canvas>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @stop

    @section('post_script')
        {!! Html::script('js/Chart.bundle.min.js') !!}
        {!! Html::script('js/date/bootstrap-datepicker.js') !!}
        {!! Html::script('js/date/bootstrap-datepicker.es.min.js') !!}
        <script>
            var api_base_route= "{!! url('api') !!}";
            var public_base_route= "{!! asset('') !!}";

            $(document).ready(function() {
                $('.input-daterange').datepicker({
                    language: 'es',
                    format: 'yyyy-mm-dd',
                    todayHighlight: true
                });
            });
        </script>
    @stop
    @else
        @section('content_title')
            Estadísticas
        @stop

        @section('bread')
            <li><a href="{!! url('system/dashboard') !!}">Dashboard</a></li>
            <li><a href="{!! url('system/businesses/') !!}">Empresas</a></li>
            <li class="active">Estadísticas</li>
        @stop
        @section('content')
            <div class="alert alert-danger">
                <strong>No ha sido registrada aún ninguna empresa</strong>
                <p>
                    Si desea acceder a la creación de reportes, visualización de estadísticas y comprobación de resultados.
                    Debe por lo menos crear una Empresa.
                </p>
                <br>
                <a href="{!! url('system/businesses/create') !!}" class="btn btn-success btn-sm" title="Crear Empresa"><i class="fa fa-pencil"></i></a>
                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-user-md" title="Contactar a Soporte Técnico"></i></a>
            </div>
        @stop

    @endif