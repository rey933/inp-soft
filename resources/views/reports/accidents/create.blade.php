@extends('layouts.master')
@if($businesses->count() > 0)
@section('post_style')
    {!! Html::style('css/wizard.css') !!}
    <style>
        .wizard.vertical > .content {
            height: 600px !important;
        }
        .wizard > .steps .current a,
        .wizard > .steps .current a:hover,
        .wizard > .steps .current a:active
        {
            background: #2870be;
            color: #fff;
            cursor: default;
        }

        .wizard > .steps .done a,
        .wizard > .steps .done a:hover,
        .wizard > .steps .done a:active
        {
            background: #287646;
            color: #fff;
        }

        .wizard > .actions a,
        .wizard > .actions a:hover,
        .wizard > .actions a:active
        {
            background: #2870be;
            color: #fff;
            display: block;
            padding: 0.5em 1em;
            text-decoration: none;

            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
    </style>
    {!! Html::style('css/date/bootstrap-datepicker.css') !!}
    {!! Html::style('wickedpicker/stylesheets/wickedpicker.css') !!}
@stop
@section('content_title')
    Reporte de Accidente Laboral
@stop

@section('bread')
    <li><a href="{!! url('system/dashboard') !!}">Dashboard</a></li>
    <li><a href="{!! url('system/reports/') !!}">Reportes</a></li>
    <li class="active">Reporte de Accidente Laboral</li>
@stop

@section('content')
    {!! Form::open(['name'=> 'accident', 'id'=> 'accident-form', 'action'=> 'Reports\AccidentsController@store']) !!}
    <div id="wizard">
        <h1>Identificación del Trabajador</h1>
        <div>
            <is-employees-search></is-employees-search>
        </div>

        <h1>Ubicación del Suceso</h1>
        <div>
            <div class="form-group">
                <is-businesses-select list=""
                                      :headquarters.sync="headquarters">
                </is-businesses-select>
            </div>
            <div class="form-group"
                 id="headquarter"
                 style="display: none;">
                <is-headquarters-select :list="headquarters"
                                        :blocks.sync="blocks">
                </is-headquarters-select>
            </div>
            <div class="form-group"
                 id="block"
                 style="display: none;">
                <is-blocks-select :list="blocks"
                                  :departments.sync="departments">
                </is-blocks-select>
            </div>
            <div class="form-group"
                 id="department"
                 style="display: none;">
                <is-departments-select :list="departments"
                                       :areas.sync="areas">

                </is-departments-select>
            </div>
            <div class="form-group"
                 id="area"
                 style="display: none;">
                <is-areas-select :list="areas"> </is-areas-select>
            </div>
            <div class="form-group"
                 id="place"
                 style="display: none;">
                {!! Form::label('place', 'Lugar del Suceso') !!}
                {!! Form::text('place', NULL, ['id'=> 'place-field', 'class'=> 'form-control required']) !!}
            </div>
        </div>

        <h1>Información del Suceso</h1>

        <div>
            <div class="form-group">
                {!! Form::label('accident_date', 'Fecha de Ocurrencia') !!}
                {!! Form::text('accident_date', NULL, ['id'=> 'accident_date', 'class'=> 'form-control required', 'readonly']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('accident_hour', 'Hora de Ocurrencia') !!}
                {!! Form::text('accident_hour', NULL, ['id'=> 'accident_hour', 'class'=> 'form-control required', 'readonly']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('accident_classification', 'Clasificación del Accidente') !!}
                {!! Form::select('accident_classification', ['LESIÓN CPT'=> 'LESIÓN CPT', 'LESIÓN SPT'=> 'LESIÓN SPT'], NULL, ['class'=> 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('accident_category', 'Categoría del Accidente') !!}
                {!! Form::select('accident_category', ['EN PLANTA'=> 'EN PLANTA', 'IN-ITINERE'=> 'IN-ITINERE', 'DEPORTIVO'=> 'DEPORTIVO', 'BRIGADA'=> 'BRIGADA'], NULL, ['class'=> 'form-control']) !!}
            </div>

            <p>Esta información va relacionada directamente con el suceso.</p>
        </div>

        <h1>Descripción del Suceso</h1>

        <div>
            <div class="form-group">
                {!! Form::label('place_type', 'Tipo de Lugar') !!}
                {!! Form::select('place_type', $place_types, NULL, ['class'=> 'form-control']) !!}
                <br>
                {!! Form::text('specific_place_type', NULL, ['class'=> 'form-control required', 'placeholder'=> 'Tipo de Lugar Específico']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('physical_activity_type', 'Actividad Física') !!}
                {!! Form::select('physical_activity_type', $physical_activity_types, NULL, ['class'=> 'form-control']) !!}
                <br>
                {!! Form::text('specific_activity_type', NULL, ['class'=> 'form-control required', 'placeholder'=> 'Tipo de Actividad Específica']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('job_type', 'Tipo de Trabajo') !!}
                {!! Form::select('job_type', $job_types, NULL, ['class'=> 'form-control']) !!}
                <br>
                {!! Form::text('specific_job_type', NULL, ['class'=> 'form-control required', 'placeholder'=> 'Tipo de Trabajo Específico']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('material_agent', 'Agente Material') !!}
                {!! Form::select('material_agent', $material_agent_types, NULL, ['class'=> 'form-control']) !!}
                <br>
                {!! Form::text('specific_material_agent', NULL, ['class'=> 'form-control required', 'placeholder'=> 'Tipo de Agente Material Específico']) !!}
            </div>
        </div>

        <h1>Afectaciones</h1>

        <div>

            <div class="form-group">
                {!! Form::label('accident_type', 'Tipo de Accidente') !!}
                {!! Form::select('accident_type', $accident_types, NULL, ['class'=> 'form-control']) !!}
                <br>
                {!! Form::text('specific_accident_type', NULL, ['class'=> 'form-control required', 'placeholder'=> 'Tipo de Accidente Específico']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('body_part', 'Parte del Cuerpo Afectada') !!}
                {!! Form::select('body_part', $body_part_types, NULL, ['class'=> 'form-control']) !!}
                <br>
                {!! Form::text('specific_body_part', NULL, ['class'=> 'form-control required', 'placeholder'=> 'Parte del Cuerpo Específica']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('injure', 'Lesión') !!}
                {!! Form::select('injure', $injure_types, NULL, ['class'=> 'form-control']) !!}
                <br>
                {!! Form::text('specific_injure', NULL, ['class'=> 'form-control required', 'placeholder'=> 'Tipo de Lesión Específica']) !!}
            </div>

        </div>

        <h1>Reposo</h1>

        <div>
            <div class="form-group">
                {!! Form::label('severity', 'Gravedad') !!}
                {!! Form::select('severity', ['LEVE'=> 'LEVE', 'MODERADO'=> 'MODERADO', 'GRAVE'=> 'GRAVE', 'MUY GRAVE'=> 'MUY GRAVE', 'MORTAL'=> 'MORTAL'], NULL, ['class'=> 'form-control']) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('repose_days', 'Días de Reposo') !!}
                {!! Form::text('repose_days', NULL, ['class'=> 'form-control required']) !!}
            </div>

            <br>

            <img src="{!! asset('img/content-images/accidents/repose.jpg') !!}" alt=""
                 style="width: 300px; display: block; margin: 0 auto;">
            <p align="center">Los días de reposo serán otorgados, según sea la gravedad del accidente.</p>
        </div>
    </div>

    <!--Components Templates-->
    <template id="is-businesses-select-template">
        {!! Form::label('business', 'Empresa') !!}
        <select name="business_id"
                class="form-control"
                id="business"
        @change="searchHeadquarters()">
        <option selected disabled>
            ------ Elige una empresa de la lista ------
        </option>
        <option v-for="business in list"
                value="@{{business.id}}">
            @{{business.name}}
        </option>
        </select>
    </template> <!--#is-businesses-select-template-->

    <template id="is-headquarters-select-template">
        {!! Form::label('headquarters', 'Sucursal') !!}
        <select name="headquarter_id"
                class="form-control col col-md-10"
                id="headquarter_id"
        @change="searchBlocks()">
        <option selected disabled>
            ------- Elige una sucursal de la lista ------
        </option>
        <option v-for="headquarter in list"
                value="@{{headquarter.id}}">
            @{{headquarter.name}}
        </option>
        </select>
    </template> <!--#is-headquarters-select-template-->

    <template id="is-blocks-select-template">
        {!! Form::label('blocks', 'Bloque') !!}
        <select name="block_id"
                class="form-control"
                id="block_id"
                v-show="list.length"
        @change="searchDepartments()">
        <option selected disabled>
            ------- Elige un bloque de la lista ------
        </option>
        <option v-for="block in list"
                value="@{{block.id}}">
            @{{block.name}}
        </option>
        </select>
        <p class="error"
           v-else>
            Error 1x0003: La sucursal seleccionada no posee Bloques asociados. Por favor contacte al Administrador del sistema. <a href="#">Contactar</a>
        </p>
    </template> <!--#is-blocks-select-template-->

    <template id="is-departments-select-template">
        {!! Form::label('departments', 'Departamento') !!}
        <select name="department_id"
                class="form-control"
                id="department_id"
                v-show="list.length"
        @change="searchAreas()">
        <option selected disabled>
            ------- Elige un departamento de la lista ------
        </option>
        <option v-for="department in list"
                value="@{{department.id}}">
            @{{department.name}}
        </option>
        </select>
        <p class="error"
           v-else>
            Error 1x0004: El Bloque seleccionado no posee Departamentos asociados. Por favor contacte al Administrador del sistema. <a href="#">Contactar</a>
        </p>
    </template> <!--#is-departments-select-template-->

    <template id="is-areas-select-template">
        {!! Form::label('areas', 'Área') !!}
        <select name="area_id"
                class="form-control"
                id="area_id"
                v-show="list.length"
        @change="showPlace()">
        <option selected disabled>
            ------- Elige un departamento de la lista ------
        </option>
        <option v-for="area in list"
                value="@{{area.id}}">
            @{{area.name}}
        </option>
        </select>
        <p class="error"
           v-else>
            Error 1x0005: El Departamento seleccionado no posee Áreas asociadas. Por favor contacte al Administrador del sistema. <a href="#">Contactar</a>
        </p>
    </template> <!--#is-areas-select-template-->

    <template id="is-employees-search-template">
        <div class="form-inline">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-child"></i></div>
                    {!! Form::text('employee', NULL, ['id'=> 'employee-identity',
                                                      'class'=> 'form-control',
                                                      'placeholder'=> 'Cédula: 12478526',]) !!}
                </div>
                <div class="input-group">
                    <button class="btn btn-warning btn-sm"
                            @click.prevent="searchEmployee()">Buscar Empleado
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </div>
        <br>
        <div id="employee-data"
             class="well"
             style="display:none;"
             role="alert"><strong>Nombre:</strong><span class="full-name"></span>
        </div>
        <div id="employee-error"
             class="alert alert-danger"
             style="display: none;"
             role="alert">
            <strong>Error:</strong> no se ha encontrado al empleado especificado
        </div>

        <!-- Hidden employee data-->
        <input type="hidden" id="hidden-employee-data">
        <input type="hidden" id="hidden-employee-result" value="false">
    </template> <!--#is-employees-search-->

    {!! Form::close() !!}


@stop

@section('post_script')
    <script>
        var api_base_route= "{!! url('api') !!}";
    </script>
    {!! Html::script('js/jquery.steps.js') !!}
    {!! Html::script('js/localization/messages_es.js') !!}
    {!! Html::script('js/date/bootstrap-datepicker.js') !!}
    {!! Html::script('js/date/bootstrap-datepicker.es.min.js') !!}
    {!! Html::script('wickedpicker/src/wickedpicker.js') !!}
    <script>
        var form= $('#accident-form');
        var wizard= $('#wizard').steps({
            labels: {
                cancel: 'Cancelar',
                current: 'Actual',
                next: 'Siguiente',
                previous: 'Anterior',
                loading: 'Cargando',
                finish: 'Finalizar',
            },
            transitionEffect: "slideLeft",
            stepsOrientation: 1 ,
            onStepChanging: function(event, currentIndex, newIndex) {
                // Allways allow previous action even if the current form is not valid!
                if (currentIndex > newIndex) {
                    return true;
                }
                if(currentIndex==0) {
                    var employee_data= $('#hidden-employee-data').val();
                    var employee_result= $('#hidden-employee-result').val();
                    if(employee_data == '' || employee_result == 'false') {
                        alert('Debe buscar a un Empleado para generar el Reporte');
                        return false;
                    }
                }
                if(currentIndex==1) {
                    var place= $('#place-field').val();
                    if(place == '') {
                        alert('Debe Especificar el lugar del Suceso');
                        return false;
                    }
                }
                // Needed in some cases if the user went back (clean up)
                if (currentIndex < newIndex)
                {
                    // To remove error styles
                    form.find(".body:eq(" + newIndex + ") label.error").remove();
                    form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                }
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                form.removeAttr('onsubmit');
                form.submit();
            }
        });

        $('#accident_date').datepicker({
            format: 'yyyy-mm-dd',
            language: 'es'
        });

        //TimePicker
        var options= {
            twentyFour: true,
            title: "Hora del Suceso",
            showSeconds: true
        };
        $('#accident_hour').wickedpicker(options);
    </script>
@stop
@else
@section('content_title')
    Reporte de Hallazgo
@stop

@section('bread')
    <li><a href="{!! url('system/dashboard') !!}">Dashboard</a></li>
    <li><a href="{!! url('system/reports/') !!}">Reportes</a></li>
    <li class="active">Reporte de Accidente</li>
@stop
@section('content')
    <div class="alert alert-danger">
        <strong>No ha sido registrada aún ninguna empresa</strong>
        <p>
            Si desea acceder a la creación de reportes, visualización de estadísticas y comprobación de resultados.
            Debe por lo menos crear una Empresa.
        </p>
        <br>
        <a href="{!! url('system/businesses/create') !!}" class="btn btn-success btn-sm" title="Crear Empresa"><i class="fa fa-pencil"></i></a>
        <a href="#" class="btn btn-default btn-sm"><i class="fa fa-user-md" title="Contactar a Soporte Técnico"></i></a>
    </div>
@stop
@endif