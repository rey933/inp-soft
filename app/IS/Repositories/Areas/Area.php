<?php

namespace App\IS\Repositories\Areas;

use App\IS\Repositories\Base\BaseModel;
use App\IS\Repositories\Reports\Finding;
use Illuminate\Database\Eloquent\Model;

class Area extends BaseModel
{
    protected $table= 'areas';
    protected $fillable= [
    	'name', 'description',
    ];

    public function departments()
    {
    	return $this->belongsToMany('App\IS\Repositories\Departments\Department', 'area_department', 'area_id', 'department_id');
    }

    public function findings()
    {
        return $this->hasMany(Finding::class, 'area_id');
    }
    
}
