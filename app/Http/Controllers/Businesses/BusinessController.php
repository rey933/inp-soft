<?php

namespace App\Http\Controllers\Businesses;

use App\IS\Repositories\Businesses\BusinessRepo;
use App\IS\Repositories\Headquarter\HeadquarterRepo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BusinessController extends Controller
{
    protected $business;
    protected $headquarter;

    public function __construct(BusinessRepo $business, HeadquarterRepo $headquarter)
    {
        $this->business= $business;
        $this->headquarter= $headquarter;
    }

    protected function businessStoreRules()
    {
        return $rules= [
            'business_name'=> 'required|unique:businesses,name|max:255',
            'business_rif'=> 'required|unique:businesses,rif',
            'business_logo'=> 'image|max:600',
            'business_description'=> 'required|max:255',
        ];
    }

    protected function businessUpdateRules($record)
    {
        return $rules= [
            'business_name'=> 'required|unique:businesses,name,'.$record->id.'|max:255',
            'business_rif'=> 'required|unique:businesses,rif,'.$record->id,
            'business_logo'=> 'image|max:600',
            'business_description'=> 'required|max:255',
        ];
    }

    protected function headquarterStoreRules()
    {
        return $rules= [
            'headquarter_name'=> 'required|unique:businesses,name|max:255',
            'headquarter_description'=> 'required|max:255',
            'headquarter_direction'=> 'required|max:255'
        ];
    }

    /**
     * Display all created Businesses.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $businesses= $this->business->all();
        return view('businesses/index', compact('businesses'));
    }

    /**
     * Return all created Businesses.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $businesses= $this->business->all();
        return $businesses;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('businesses/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->businessStoreRules()); //Validating the request
        if($request->hasFile('business_logo')) {
            $business= $this->business->create([
                'name'=> $request->business_name,
                'rif'=> $request->business_rif,
                'description'=> $request->business_description,
            ]);
            $image_name= $business->id.'.'.$request->file('business_logo')
                                                   ->getClientOriginalExtension(); //Getting logo name
            $request->file('business_logo')
                    ->move(base_path() . '/public/img/content-images/businesses/logos', $image_name); //Moving the file
            $business->image= $image_name;
            $business->save();
        } else {
            $business= $this->business->create([
                'name'=> $request->business_name,
                'rif'=> $request->business_rif,
                'description'=> $request->business_description,
            ]);
        }

        //Checking if we need to create a new Headquarter
        if(!$request->headquarters_advice) {
            $this->validate($request, $this->headquarterStoreRules()); //Validating request
            $headquarter= $this->headquarter->create([
                'business_id'=> $business->id,
                'name'=> $request->headquarter_name,
                'description'=> $request->headquarter_description,
                'direction'=> $request->headquarter_direction,
            ]);
        }

        return redirect()->to('system/businesses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $business= $this->business->search($id);
        return view('businesses/edit', compact('business'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $business= $this->business->search($id);
        $this->validate($request, $this->businessUpdateRules($business)); //Validating the request

        if($request->hasFile('business_logo')) {
            $business->update([
                'name'=> $request->business_name,
                'rif'=> $request->business_rif,
                'description'=> $request->business_description,
            ]);
            $image_name= $business->id.'.'.$request->file('business_logo')
                    ->getClientOriginalExtension(); //Getting logo name
            $request->file('business_logo')
                ->move(base_path() . '/public/img/content-images/businesses/logos', $image_name); //Moving the file
            $business->image= $image_name;
            $business->save();
        } else {
            $business->update([
                'name'=> $request->business_name,
                'rif'=> $request->business_rif,
                'description'=> $request->business_description,
            ]);
        }

        return redirect()->to('system/businesses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display Statistics Business Page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|int
     */
    public function statistics()
    {
        $businesses= $this->business->all();
        return view('businesses/statistics', compact('businesses'));
    }
}
