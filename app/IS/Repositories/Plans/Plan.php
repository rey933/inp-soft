<?php

namespace App\IS\Repositories\Plans;

use App\IS\Repositories\Base\BaseModel;

class Plan extends BaseModel
{
    protected $table= 'plans';
    protected $fillable= [
    	'finding_id', 'title', 'status',
    ];

    public function finding()
    {
    	return $this->belongsTo('App\IS\Repositories\Reports\Finding', 'finding_id');
    }

    public function tasks()
    {
    	return $this->hasMany('App\IS\Repositories\Tasks\Task', 'plan_id');
    }
    
}