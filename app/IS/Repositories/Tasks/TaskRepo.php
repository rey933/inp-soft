<?php
/**
 * Created by Carlos José Rey Sayago.
 * User: crey
 * Date: 15/09/2016
 * Time: 07:39 AM
 */

namespace App\IS\Repositories\Tasks;


use App\IS\Repositories\Base\BaseRepo;

class TaskRepo extends BaseRepo
{

    public function getModel()
    {
        return new Task();
    }

    public function getTaskPlan()
    {
    	$plan= $this->getModel()->plan()->get();
    	return $plan;
    }

    public function getTaskFinding()
    {
    	$plan= $this->getTaskPlan();
    	$finding= $plan->finding()->get();
    	return $finding;
    }
}