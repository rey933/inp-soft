<?php

use Illuminate\Database\Seeder;

class InpsaselMaterialAgentsTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'NINGÚN AGENTE MATERIAL O NINGUNA INFORMACIÓN',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'EDIF / SUPERFIC / MISMO NIVEL',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'EDIF / CONTRUCC / SUPERFIC ALTURA',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'EDIF / CONSTRUCC / SUPERFICS DEBAJO NIVEL DEL SUELO',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'DISP DE DISTRIB DE MATERIA, DE ALIMENTACIÓN, CANALIZACIONES',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'MOTORES , DISP TRANSM Y ALMACENAM ENERGÍA',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'HERRAMIENTAS MANUALES SIN MOTOR',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'HERRAM MECÁNICAS SOSTENIDAS O GUIADAS CON LAS MANOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'HERRAM MANUALES, SIN ESPECIF DE MOTORIZACIÓN',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'MÁQUINAS Y EQUIPOS PORTÁTILES O MÓVILES',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'MÁQUINAS Y EQUIPOS FIJOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'DISP TRASLADO, TRANSPORTE Y ALMACENAMIENTO',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'VEHÍCULOS TERRESTRES',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'OTROS VEHÍCULOS DE TRANSPORTE',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'MATERIALES, OBJETOS, PRODUCTOS, ELEM CONST DE VEHÍCULO , FRAGMENTOS, POLVOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'SUST QUÍMICAS, EXPLOSIVAS, RADIACTIVAS, BIOLÓGICOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'DISP Y EQUIPOS DE PROTECCIÓN',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'EQUIPOS DE OFICINA Y PERSONALES, MATERIAL DE DEPORTE, ARMAS, APARATOS DE DOMÉSTICOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'ORGANISMOS VIVOS Y SERES HUMANOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'RESIDUOS EN GRANDES CANTIDADES',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'FENÓMENOS FÍSICOS Y ELEMENTOS NATURALES',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-material-agents-type')->insert([
            'title'=> 'OTROS AGENTES MATERIALES NO CITADOS EN ESTA CLASIFICACIÓN',
            'inpsasel-code'=> 1,
        ]);
    }
}
