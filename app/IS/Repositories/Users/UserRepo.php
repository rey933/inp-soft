<?php
/**
 * Created by Carlos José Rey Sayago.
 * User: cjrey
 * Date: 2016/09/10
 * Time: 15:17
 */

namespace App\IS\Repositories\Users;

use App\IS\Repositories\Base\BaseRepo;
use App\User;

class UserRepo extends BaseRepo
{
	public function getModel()
	{
		return new User();
	}
}