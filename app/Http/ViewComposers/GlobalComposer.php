<?php

namespace App\Http\ViewComposers;

use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

/**
 * Created by Carlos José Rey Sayago.
 * User: crey
 * Date: 15/09/2016
 * Time: 02:22 PM
 */
class GlobalComposer
{
    public function compose(View $view)
    {
        $view->with('user', Auth::user());
    }
}