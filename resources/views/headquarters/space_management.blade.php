@extends('layouts.master')
    @section('post_style')
        <style>
            #space_manager > div {
                margin-bottom: 50px;
            }
            .section-image {
                height: 40px;
                background: center no-repeat;
            }
            .section-title > h4{
                text-align: center;
            }
            .section-content  a{
                cursor: pointer;
            }
        </style>
    @stop
    @section('content_title')
        Distribución del espacio
    @stop

    @section('bread')
        <li><a href="{!! url('system/dashboard') !!}">Dashboard</a></li>
        <li><a href="{!! url('system/businesses/') !!}">Empresas</a></li>
        <li><a href="{!! url('system/headquarters/'.$headquarter->id) !!}">Sucursales</a></li>
        <li>Distribución del Espacio en la Sucursal: {!! $headquarter->name !!}</li>
    @stop

    @section('content')
        <div id="space_manager" class="row">
            <div id="init-error" class="alert alert-danger" style="display: none">
                Ha ocurrido un error en el <b>Servidor</b> que ha impedido la inicialización de la aplicación
            </div>
            <div id="general-error" class="alert alert-danger" style="display: none">
                Ha ocurrido un error en el <b>Servidor</b> que ha impedido la ejecución de la operación
            </div>
            <div class="col-md-3 col-xs-6"
                 id="blocks"
                 style="display:none">
                <div class="section-image"
                     style="background-image: url('{!! asset('img/content-images/businesses/management/blocks.jpg') !!}')">
                </div>
                <div class="section-title">
                    <h4>Bloques (<span class="counter">@{{ blocks.length }}</span>)</h4>
                </div>
                <div class="section-content">
                    <is-blocks :list.sync="blocks"
                                headquarter_id="{!! $headquarter->id !!}"
                               :active_id.sync="active_block" 
                               :departments_list.sync="departments">
                                   
                    </is-blocks>
                </div>
            </div>
            <div class="col-md-3 col-xs-6"
                 id="departments"
                 style="display: none;">
                <div class="section-image"
                     style="background-image: url('{!! asset('img/content-images/businesses/management/departments.jpg') !!}')">
                </div>
                <div class="section-title">
                    <h4>Departamentos (<span class="counter">@{{ departments.length }}</span>)</h4>
                </div>
                <div class="section-content">
                    <is-departments :list.sync="departments"
                                    :added_areas_list.sync="added_areas"
                                    :active_id.sync="active_department">                                        
                    </is-departments>
                </div>
            </div>
            <div class="col-md-3 col-xs-6"
                 id="areas"
                 style="display:none">
                <div class="section-image"
                     style="background-image: url('{!! asset('img/content-images/businesses/management/areas.png') !!}')">
                </div>
                <div class="section-title">
                    <h4>Áreas (<span class="counter">@{{ added_areas.length }}</span>)</h4>
                </div>
                <div class="section-content">
                    <is-areas :list.sync="areas"
                              :added_areas_list.sync="added_areas"
                              :places_list.sync="places"
                              :active_department_id="active_department">                                        
                    </is-areas>
                </div>

            </div>
            <div class="col-md-3 col-xs-6"
                 id="places"
                 style="display:none">
                <div class="section-image"
                     style="background-image: url('{!! asset('img/content-images/businesses/management/places.png') !!}')">
                </div>
                <div class="section-title">
                    <h4>Lugares (<span class="counter">@{{places.length}}</span>)</h4>
                </div>
                <div class="section-content">
                    <is-places :list="places"></is-places>
                </div>
            </div>
        </div> <!-- /#space_manager-->

        <!--Including modal components-->
        <manage-blocks-modal :list="blocks" 
                              headquarter_id="{!! $headquarter->id !!}">
        </manage-blocks-modal>
        <manage-departments-modal :list="departments"
                                  :active_id="active_block">
        </manage-departments-modal>
        <manage-areas-modal :list="areas"
                            :active_id="active_area"
                            :added_areas_list.sync="added_areas">
        </manage-areas-modal>

        <!--Components templates-->
            <!--is-blocks-template-->
            <template id="is-blocks-template">
                <div class="list-group" v-show="list.length">
                    <a href="#!" 
                       class="list-group-item option" 
                       v-for="block in list"
                       @click="searchDepartments(block)">
                        @{{ block.name }}
                    </a>
                </div>
                <p align="center" v-else>No ha creado bloques para esta sucursal</p>
                <a href="#!" data-toggle="modal" data-target="#manage-blocks-modal">Administrar Bloques</a>
            </template> <!-- /#is-blocks-template-->

            <!--is-departments-template-->
            <template id="is-departments-template">
                <div class="list-group" v-show="list.length">
                    <a href="#!" class="list-group-item" 
                       v-for="department in list"
                       @click="searchAreas(department.id)">
                       @{{ department.name }}
                    </a>
                </div>
                <p align="center" v-else>No ha asociado ningún departamento a este bloque</p>
                <a href="#!" data-toggle="modal" data-target="#manage-departments-modal">Administrar Departamentos</a>
            </template> <!-- /#is-departments-template-->

            <!--is-areas-template-->
            <template id="is-areas-template">
                <div class="form-inline" style="text-align: center;" v-show="list.length">
                    {!! Form::open(['method'=> 'POST', 'id'=> 'assign-area-form',
                                                'onsubmit'=> 'return false']) !!}
                        <div class="form-group">
                            <select id="area-select">
                                <option value="@{{area.id}}" v-for="area in list">@{{ area.name }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                        <button class="btn btn-warning btn-sm" title="Asignar área al departamento" @click="attachArea()"><i class="fa fa-plus"></i></button>
                        </div>
                    {!! Form::close() !!}
                </div>
                <br>
                <div class="list-group" v-show="added_areas_list.length">
                    <ul class="list-group">
                        <li class="list-group-item" 
                            v-for="area in added_areas_list"
                            style="text-align: center;">
                            @{{area.name}}                           
                            <i class="fa fa-eye" 
                               title="Desplegar los Lugares reportados para esta área."
                               style="cursor: pointer;"
                               @click="searchPlaces(area.id)"></i>
                            <i class="fa fa-minus" 
                               title="Disociar esta área de éste departamento." 
                               style="color: #d9534f; cursor: pointer;"
                               @click="detachArea(area, area.id)"></i>
                        </li>
                    </ul>
                </div>
                <p align="center" v-show="!added_areas_list.length && list.length">No ha asociado ningún Área a este departamento</p>
                <p align="center" v-show="!list.length">No ha creado áreas en la base de datos. Use el Administrador para crear algunas.</p>
                <a href="#!" data-toggle="modal" data-target="#manage-areas-modal">Administrar Áreas</a>
            </template> <!-- /#is-areas-template-->

             <!--is-places-template-->
            <template id="is-places-template">
                <div v-show="list.length">
                    <ul class="list-group">
                        <li v-for="place in list">@{{place.name}}</li>
                    </ul>
                </div>
                <p align="center" v-else>No se ha reportado ninguna incidencia en el área</p>
            </template> <!-- /#is-places-template-->

            <!--manage-blocks-modal-->
            <template id="manage-blocks-modal">
                <div class="modal fade" id="manage-blocks-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Administrador de Bloques</h4>
                      </div>
                      <div class="modal-body">
                        <table class="table table-striped">
                            <thead>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Acciones</th>
                            </thead>
                            <tbody>
                                <tr v-for="block in list">
                                    <td>@{{ block.name }}</td>
                                    <td>@{{ block.description }}</td>
                                    <td>
                                        <button class="btn btn-danger btn-sm"
                                                title="Eliminar Bloque"
                                                @click="removeBlock(block)">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div>
                            <div class="form-inline">
                                {!! Form::open(['method'=> 'POST', 'id'=> 'create-block-form',
                                                'onsubmit'=> 'return false']) !!}
                                    <div class="form-group">
                                        {!! Form::text('block_name', NULL, ['class'=> 'form-control', 
                                                                            'placeholder'=> 'Nombre del bloque',
                                                                            'id'=> 'block-name']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::text('block_description', NULL, ['class'=> 'form-control', 
                                                                                   'placeholder'=> 'Descripción del bloque',
                                                                                   'id'=> 'block-description']) !!}
                                    </div>                            
                                    <div class="form-group">
                                        <button class="btn btn-success btn-sm" title="Crear" @click="createBlock()"><i class="fa fa-share"></i></button>
                                    </div>
                                {!! Form::close() !!}                    
                            </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                      </div>
                    </div>
                  </div>
                </div>
            </template> <!-- /#manage-blocks-modal-->

            <!--manage-departments-modal-->
            <template id="manage-departments-modal">
                <div class="modal fade" id="manage-departments-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Administrador de Departamentos</h4>
                      </div>
                      <div class="modal-body">
                        <table class="table table-striped">
                            <thead>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Acciones</th>
                            </thead>
                            <tbody>
                                <tr v-for="department in list">
                                    <td>@{{ department.name }}</td>
                                    <td>@{{ department.description }}</td>
                                    <td>
                                        <button class="btn btn-danger btn-sm"
                                                title="Eliminar Bloque"
                                                @click="removeDepartment(department)">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div>
                            <div class="form-inline">
                                {!! Form::open(['method'=> 'POST', 'id'=> 'create-department-form',
                                                'onsubmit'=> 'return false']) !!}
                                    <div class="form-group">
                                        {!! Form::text('department_name', NULL, ['class'=> 'form-control', 
                                                                            'placeholder'=> 'Nombre del Departamento',
                                                                            'id'=> 'department-name']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::text('department_description', NULL, ['class'=> 'form-control', 
                                                                                   'placeholder'=> 'Descripción del Departamento',
                                                                                   'id'=> 'department-description']) !!}
                                    </div>                            
                                    <div class="form-group">
                                        <button class="btn btn-success btn-sm" title="Crear" @click="createDepartment()"><i class="fa fa-share"></i></button>
                                    </div>
                                {!! Form::close() !!}                    
                            </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                      </div>
                    </div>
                  </div>
                </div>
            </template> <!-- /#manage-blocks-modal-->

            <!--manage-areas-modal-->
            <template id="manage-areas-modal">
                <div class="modal fade" id="manage-areas-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Administrador de Áreas</h4>
                      </div>
                      <div class="modal-body">                        
                        <table class="table table-striped">
                            <thead>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Acciones</th>
                            </thead>
                            <tbody>
                                <tr v-for="area in list">
                                    <td>@{{ area.name }}</td>
                                    <td>@{{ area.description }}</td>
                                    <td>
                                        <button class="btn btn-danger btn-sm"
                                                title="Eliminar Área de la Base de Datos"
                                                @click="removeArea(area)">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div>
                            <div class="form-inline">
                                {!! Form::open(['method'=> 'POST', 'id'=> 'create-area-form',
                                                'onsubmit'=> 'return false']) !!}
                                    <div class="form-group">
                                        {!! Form::text('area_name', NULL, ['class'=> 'form-control', 
                                                                            'placeholder'=> 'Nombre del Área',
                                                                            'id'=> 'area-name']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::text('area_description', NULL, ['class'=> 'form-control', 
                                                                                   'placeholder'=> 'Descripción del Área',
                                                                                   'id'=> 'area-description']) !!}
                                    </div>                            
                                    <div class="form-group">
                                        <button class="btn btn-success btn-sm" title="Crear" @click="createArea()"><i class="fa fa-share"></i></button>
                                    </div>
                                {!! Form::close() !!}                    
                            </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                      </div>
                    </div>
                  </div>
                </div>
            </template> <!-- /#manage-areas-modal-->
    @stop

    @section('post_script')
        <script>           
           var api_base_route= "{!! url('api') !!}";
        </script>
    @stop