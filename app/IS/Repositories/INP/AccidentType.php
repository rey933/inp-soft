<?php

namespace App\IS\Repositories\INP;

use Illuminate\Database\Eloquent\Model;

class AccidentType extends Model
{
    protected $table= 'inpsasel-accidents-type';
}
