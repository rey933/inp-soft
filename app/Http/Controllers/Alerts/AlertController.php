<?php

namespace App\Http\Controllers\Alerts;

use App\Http\Controllers\Auth\AuthController;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AlertController extends Controller
{
    protected $user;
    protected $auth;

    public function __construct(AuthController $auth)
    {
        $this->middleware('auth');
        $this->auth= $auth;
    }

    public function index()
    {
        //$this->auth->logout();
        $is_logged= $this->auth->isLogged();
        if($is_logged) {
            return 'Logged in';
        } else {
            return 'Is not Logged in';
        }
    }
}
