<?php

use Illuminate\Database\Seeder;

class InpsaselAccidentsTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inpsasel-accidents-type')->insert([
            'title'=> 'EXPOSICIÓN O CONTACTO CON AGENTES FÍSICOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-accidents-type')->insert([
            'title'=> 'CONTACTO CON SUSTANCIAS NOCIVAS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-accidents-type')->insert([
            'title'=> 'QUEDAR SEPULTADO / ENVUELTO / INMERSO  LÍQUIDOS, GASES, PARTÍCULAS EN SUSPENSIÓN',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-accidents-type')->insert([
            'title'=> 'CAÍDAS DE PERSONAS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-accidents-type')->insert([
            'title'=> 'CHOQUE CONTRA EL AGENTE MATERIAL',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-accidents-type')->insert([
            'title'=> 'GOLPEADO POR OBJETOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-accidents-type')->insert([
            'title'=> 'PISAR SOBRE',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-accidents-type')->insert([
            'title'=> 'CONTACTO CON AGENTE MATERIAL CORTANTE, PUNZANTE, DURO, RUGOSO',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-accidents-type')->insert([
            'title'=> 'ATRAPADO EN, DEBAJO, ENTRE O POR2',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-accidents-type')->insert([
            'title'=> 'ESFUERZOS EXCESIVOS/ MOVIMIENTOS VIOLENTOS/ CHOQUE MENTAL EXCESIVA',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-accidents-type')->insert([
            'title'=> 'MORDEDURAS, PATADAS, ETC. (DE ANIMALES O PERSONAS)',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-accidents-type')->insert([
            'title'=> 'OTRA FORMA DE ACCIDENTE NO CLASIFICADO',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-accidents-type')->insert([
            'title'=> 'OTRA FORMA DE ACCIDENTE NO CLASIFICADO EN OTROS EPIGRAFES',
            'inpsasel-code'=> 1,
        ]);
    }
}
