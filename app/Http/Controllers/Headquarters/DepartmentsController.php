<?php

namespace App\Http\Controllers\Headquarters;

use App\IS\Repositories\Departments\DepartmentRepo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DepartmentsController extends Controller
{
    protected $department;

    public function __construct(DepartmentRepo $department)
    {
        $this->department= $department;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($block_id)
    {
        
    }

    /**
      * Search Departments for the given $block_id
      */
    public function search($block_id)
    {
        return $departments= $this->department->getModel()->where('block_id', '=', $block_id)->get()->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($block_id, Request $request)
    {
        $data= $this->department->create([
            'block_id'=> $block_id,
            'name'=> $request->department_name,
            'description'=> $request->department_description,
        ]);

        return response()->json([
            'department'=> [
                'id'=> $data->id,
                'name'=> $data->name,
                'description'=> $data->description,
            ],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function delete($department_id)
    {
        $department= $this->department->search($department_id);
        $department->delete();
        return response()->json([
            'status'=> 'completed'
        ]);
    }
}
