//------------is-blocks-component--------------------
Vue.component('is-blocks', {
	props: ['list', 'headquarter_id', 'active_id', 'departments_list'],
	template: '#is-blocks-template',

	methods: {
		searchDepartments: function(block, clicked_option) {
			this.active_id= block.id;
			$('#departments').slideUp('fast');
			$('#loading_wrap').fadeIn();
			$.getJSON(api_base_route+'/departments/search/'+this.active_id, function(departments) {
				$('#loading_wrap').fadeOut();
				this.departments_list= departments;				
				$('#areas').slideUp('fast');
				$('#places').slideUp('fast');
				$('#departments').slideDown('slow');
			}.bind(this)).fail(function(e) {
				$('#general-error').slideDown;
			});
		},
	},

	created: function() {
		$('#loading_wrap').fadeIn();
		$.getJSON(api_base_route+'/blocks/search/'+this.headquarter_id, function(blocks) {
			this.list= blocks;
			$('#loading_wrap').fadeOut();
		}.bind(this)).fail(function(e) {
			$('#init-error').slideDown();
			$('#loading_wrap').fadeOut();
		});
	},
});

//------------manage-blocks-modal-component--------------------
Vue.component('manage-blocks-modal', {
	props: ['list', 'headquarter_id'],
	template: '#manage-blocks-modal',

	methods: {
		createBlock: function(e) {
			var name= $('#block-name').val();
			var description= $('#block-description').val();
			if(name && description) {
				$('#loading_wrap').fadeIn();
				var form= $('#create-block-form');
				var data= form.serialize();
				$.post(api_base_route+'/blocks/store/'+this.headquarter_id, data, function(response) {
					$('#loading_wrap').fadeOut();
					this.list.push({
						id: response.block.id,
						name: response.block.name,
						description: response.block.description,
					});
					$('#block-name').val('');
					$('#block-description').val('');
					$('#block-name').focus();
				}.bind(this));
			} else {
				alert("Debe insertar valores para los campos");
			}			
		},
		
		removeBlock: function(block) {
			$('#loading_wrap').fadeIn();
			$.get(api_base_route+'/blocks/delete/'+block.id, function() {
				$('#loading_wrap').fadeOut();
			});
			this.list.$remove(block);
			
		},
	},
});

//------------is-departments-component--------------------
Vue.component('is-departments', {
	props: ['list', 'added_areas_list', 'active_id'],
	template: '#is-departments-template',

	methods: {
		searchAreas: function(department_id) {
			$('#loading_wrap').fadeIn();
			this.active_id= department_id;
			$.getJSON(api_base_route+'/areas/search/'+department_id, function(areas) {
				$('#loading_wrap').fadeOut();
				this.added_areas_list= areas;
				$('#places').slideUp('fast');
				$('#areas').slideUp('fast');
				$('#areas').slideDown('slow');
			}.bind(this)).fail(function(e) {
				$('#general-error').slideDown;
			});
		},
	},
});

//------------manage-departments-modal-component--------------------
Vue.component('manage-departments-modal', {
	props: ['list', 'active_id'],
	template: '#manage-departments-modal',

	methods: {
		createDepartment: function(e) {
			$('#loading_wrap').fadeIn();
			var name= $('#department-name').val();
			var description= $('#department-description').val();
			if(name && description) {
				var form= $('#create-department-form');
				var data= form.serialize();
				$.post(api_base_route+'/departments/store/'+this.active_id, data, function(response) {
					$('#loading_wrap').fadeOut();
					this.list.push({
						id: response.department.id,
						name: response.department.name,
						description: response.department.description,
					});
					$('#department-name').val('');
					$('#department-description').val('');
					$('#department-name').focus();
				}.bind(this));
			} else {
				alert("Debe insertar valores para los campos");
			}			
		},
		removeDepartment: function(department) {
			$('#loading_wrap').fadeIn();
			$.get(api_base_route+'/departments/delete/'+department.id, function() {
				$('#loading_wrap').fadeOut();
			});
			this.list.$remove(department);			
		},
	},
});

//------------is-areas-component--------------------
Vue.component('is-areas', {
	props: ['list', 'added_areas_list', 'places_list', 'active_department_id'],
	template: '#is-areas-template',

	methods: {
		searchPlaces: function(area_id) {
			$('#loading_wrap').fadeIn();
			$.get(api_base_route+'/places/search/'+area_id, function(places) {
				$('#loading_wrap').fadeOut();
				$('#places').slideUp();
				$('#places').slideDown();
				this.places= places;
			}.bind(this));
		},

		attachArea: function() {
			var area_id= $('#area-select').val();
			var department_id = this.active_department_id;
			var flag= false;
			$.each(this.added_areas_list, function(index, area) {
				if(area_id == area.id) {
					flag= true;
				} 
			}.bind(this));
			if(!flag) {
				$('#loading_wrap').fadeIn();
				$.get(api_base_route+'/areas/attach/'+department_id+'/'+area_id, function(response) {
					$('#loading_wrap').fadeOut();
					this.added_areas_list.push({
						id: response.area.id,
						name: response.area.name,
						description: response.area.description,
					});
				}.bind(this));
			} else {
				alert('Ya ha sido asociada esta área');
			}
		},
		detachArea: function(area, area_id) {
			$('#loading_wrap').fadeIn();
			$('#places').slideUp('fast');
			$.get(api_base_route+'/areas/detach/'+this.active_department_id+'/'+area_id, function () {
				$('#loading_wrap').fadeOut();
			});
			this.added_areas_list.$remove(area);
		},
	},

	created: function() {
		$('#loading_wrap').fadeIn();
		$.getJSON(api_base_route+'/areas/all', function(areas) {
			$('#loading_wrap').fadeOut();
			this.list= areas;
			$('#blocks').slideDown();
		}.bind(this)).fail(function(e) {
			$('#init-error').slideDown();
		});
	},
});


//------------manage-areas-modal-component--------------------
Vue.component('manage-areas-modal', {
	props: ['list', 'active_id', 'added_areas_list'],
	template: '#manage-areas-modal',

	methods: {
		createArea: function(e) {
			var name= $('#area-name').val();
			var description= $('#area-description').val();
			if(name && description) {
				var form= $('#create-area-form');
				var data= form.serialize();
				$('#loading_wrap').fadeIn();
				$.post(api_base_route+'/areas/store', data, function(response) {
					$('#loading_wrap').fadeOut();
					this.list.push({
						id: response.area.id,
						name: response.area.name,
						description: response.area.description,
					});
					$('#area-name').val('');
					$('#area-description').val('');
					$('#area-name').focus();
				}.bind(this));
			} else {
				alert("Debe insertar valores para los campos");
			}		
		},
		removeArea: function(area) {
			$('#loading_wrap').fadeIn();
			$('#places').slideUp('fast');
			$('#areas').slideUp('fast');
			$.get(api_base_route+'/areas/delete/'+area.id, function() {
				$('#loading_wrap').fadeOut();
			});
			$.each(this.added_areas_list, function(index, added_area) {
				if(added_area.id == area.id) {
					this.added_areas_list.$remove(added_area);
				}
			}.bind(this));
			this.list.$remove(area);			

			$('#areas').slideDown('slow');

		},
	},	
});

//------------is-areas-component--------------------
Vue.component('is-places', {
	props: ['list'],
	template: '#is-places-template',
});

//------------is-businesses-select-component--------------------
Vue.component('is-businesses-select', {
	props: ['list', 'headquarters'],
	template: '#is-businesses-select-template',
	methods: {
		searchHeadquarters: function() {
			var business_id= $('#business').val();
			$('#place-field').val('');
			$('#headquarter').slideUp();
			$('#block').slideUp();
			$('#department').slideUp();
			$('#area').slideUp();
			$('#place').slideUp();
			$('#loading_wrap').fadeIn();
			$.getJSON(api_base_route+'/headquarters/search-by-business/'+business_id, function(headquarters) {
				$('#loading_wrap').fadeOut();
				this.headquarters= headquarters;
				$('#headquarter').slideDown();		
			}.bind(this)).fail(function(e) {
				$('#init-error').slideDown();
				console.log('Ha ocurrido un error');
			});
		},
	},
	created: function() {
		$('#loading_wrap').fadeIn();
		$.getJSON(api_base_route+'/businesses/all', function(businesses) {
			$('#loading_wrap').fadeOut();
			this.list= businesses;	
		}.bind(this)).fail(function(e) {
			$('#init-error').slideDown();
			console.log('Ha ocurrido un error al iniciar el asistente');
		});
	}
});

//------------is-headquarters-select-component--------------------
Vue.component('is-headquarters-select', {
	props: ['list', 'blocks'],
	template: '#is-headquarters-select-template',
	methods: {
		searchBlocks: function() {
			var headquarter_id= $('#headquarter_id').val();
			$('#place-field').val('');
			$('#block').slideUp();
			$('#department').slideUp();
			$('#area').slideUp();
			$('#place').slideUp();
			$('#loading_wrap').fadeIn();
			$.getJSON(api_base_route+'/blocks/search/'+headquarter_id, function(blocks) {
				$('#loading_wrap').fadeOut();
				this.blocks= blocks;	
				console.log(blocks);	
				$('#block').slideDown();
			}.bind(this)).fail(function(e) {
				$('#init-error').slideDown();
				console.log('Ha ocurrido un error');
			});
		},
	},
});

//------------is-blocks-select-component--------------------
Vue.component('is-blocks-select', {
	props: ['list', 'departments'],
	template: '#is-blocks-select-template',

	methods: {
		searchDepartments: function() {
			var block_id= $('#block_id').val();
			$('#place-field').val('');
			$('#department').slideUp();
			$('#area').slideUp();
			$('#place').slideUp();
			$('#loading_wrap').fadeIn();
			$.getJSON(api_base_route+'/departments/search/'+block_id, function(departments) {
				$('#loading_wrap').fadeOut();
				this.departments= departments;
				$('#department').slideDown();
			}.bind(this)).fail(function(e) {
				$('#init-error').slideDown();
				console.log('Ha ocurrido un error');
			});
		},
	},
});

//------------is-departments-select-component--------------------
Vue.component('is-departments-select', {
	props: ['list', 'areas'],
	template: '#is-departments-select-template',

	methods: {
		searchAreas: function() {
			var department_id= $('#department_id').val();
			$('#place-field').val('');
			$('#area').slideUp();
			$('#place').slideUp();
			$('#loading_wrap').fadeIn();
			$.getJSON(api_base_route+'/areas/search/'+department_id, function(areas) {
				$('#loading_wrap').fadeOut();
				this.areas= areas;
				$('#area').slideDown();
			}.bind(this)).fail(function(e) {
				$('#init-error').slideDown();
				console.log('Ha ocurrido un error');
			});
		}
	},
});

//------------is-areas-select-component--------------------
Vue.component('is-areas-select', {
	props: ['list'],
	template: '#is-areas-select-template',

	methods: {
		showPlace: function() {
			$('#place-field').val('');
			$('#place').slideUp();
			$('#place').slideDown();
		},
	},
});

//------------is-employees-search-component--------------------
Vue.component('is-employees-search', {
	props: ['list'],
	template: '#is-employees-search-template',

	methods: {
		searchEmployee: function() {
			var employee_identity= $('#employee-identity').val();
			if(employee_identity != '') {
				$('#employee-data').slideUp();
				$('#employee-error').slideUp();
				$('#loading_wrap').fadeIn();
				$.getJSON(api_base_route+'/employees/search/'+employee_identity, function(employee) {
					$('#loading_wrap').fadeOut();
					if(employee != '') {
						$('#hidden-employee-data').val(employee_identity);
						$('#hidden-employee-result').val(true);
						this.list= employee;
						$('#mandatory-identity').val(employee[0].identity);
						$('#employee-data').fadeIn();
						$('#employee-data .full-name').html(' ' + employee[0].last_name + ' ' + employee[0].name);
					} else {
						this.list= '';
						$('#mandatory-identity').val('');
						$('#hidden-employee-result').val(false);
						$('#employee-error').slideDown();
					}
				}.bind(this)).fail(function(e) {
					$('#init-error').slideDown();
					console.log('Ha ocurrido un error');
				});
			} else{
				alert('Debe rellenar el campo');
			}
		},
	},
});

//------------is-employees-search-component--------------------
Vue.component('is-action-plan', {
	props: ['list', 'plan_id'],
	template: '#is-action-plan-template',

	methods: {
		displayStatus: function(event) {
			el= event.target;
			row= $(el).parents('tr');
			select= row.children('td').children('.task-status');
			select.fadeToggle();
		},

		changeTaskStatus: function(task, event) {
			el= event.target;
			row= $(el).parents('tr');
			select= row.children('td').children('.task-status');
			new_status= select.val();

			$('#loading_wrap').fadeIn();

			$.get(api_base_route+'/reports/tasks/change-status/'+task.id+'/'+new_status, function(response) {
				task.status= new_status;
				$('#loading_wrap').fadeOut();
			}.bind(this)).fail(function(e) {
				$('#loading_wrap').fadeOut();
				alert('Error 0x0001: Error interno del servidor');
			});
		},

		removeTask: function(task) {
			$('#loading_wrap').fadeIn();
			$.get(api_base_route+'/reports/tasks/delete/'+task.id, function(response) {
				this.list.$remove(task);
				$('#loading_wrap').fadeOut();
			}.bind(this)).fail(function(e) {
				$('#loading_wrap').fadeOut();
				alert('Error 0x0001: Error interno del servidor');
			});
		},
	},

	created: function(plan_id) {
		$.getJSON(api_base_route+'/reports/tasks/search/'+this.plan_id, function(tasks) {
			this.list= tasks;			
		}.bind(this)).fail(function(e) {
			
		});
	}
});


//------------is-statistics-component--------------------
Vue.component('is-statistics', {
	props: ['businesses_list', 'headquarters_list'],
	template: '#is-statistics-template',

    methods: {
        displaySelectionScreen: function () {
            $('.business-selection').fadeOut('fast');
            $('.selection-screen').fadeIn('slow');
        },

        displayBusinessScreen: function () {
            $('.injuries-form').fadeOut();
            $('.findings-form').fadeOut();
            $('.business-selection').fadeIn();
            $.get(api_base_route + '/businesses/all', function (businesses) {
                $('#loading_wrap').fadeOut();
                $('.selection-screen').fadeOut();
                $('.business-selection').fadeIn();
                this.businesses_list = businesses
            }.bind(this)).fail(function (e) {
                $('#loading_wrap').fadeOut();
                alert('Error 0x0001: Error interno del servidor');
            });
        },

        displayInjuriesSection: function () {
            //Hide the other section
            $('#findings').fadeOut();
            $('#injuries').fadeIn();
            this.displayBusinessScreen();
        },

        displayFindingsSection: function() {
            //Hide the other section
            $('#injuries').fadeOut();
            $('#findings').fadeIn();
            this.displayBusinessScreen();
        },

        displaySearchInjuriesForm: function(business_id) {
            $('#loading_wrap').fadeIn();
            $.getJSON(api_base_route+'/headquarters/search-by-business/'+business_id, function(headquarters) {
                $('#loading_wrap').fadeOut();

                if(headquarters.length > 0) {
                    this.headquarters_list= headquarters;

                    //Displaying the form
                    $('.business-selection').fadeOut();
                    $('.injuries-form').fadeIn('slow');
                } else {
                    alert('Esta empresa no posee sucursales asociadas.');
                }
            }.bind(this)).fail(function(e) {
                $('#loading_wrap').fadeOut();
                alert('Error 0x0001: Error interno del servidor');
            });
        },

        displaySearchFindingsForm: function(business_id) {
            $('#loading_wrap').fadeIn();
            $.getJSON(api_base_route+'/headquarters/search-by-business/'+business_id, function(headquarters) {
                $('#loading_wrap').fadeOut();

                if(headquarters.length > 0) {
                    this.headquarters_list= headquarters;

                    //Displaying the form
                    $('.business-selection').fadeOut();
                    $('.findings-form').fadeIn('slow');
                } else {
                    alert('Esta empresa no posee sucursales asociadas.');
                }
            }.bind(this)).fail(function(e) {
                $('#loading_wrap').fadeOut();
                alert('Error 0x0001: Error interno del servidor');
            });
        },

        getFindingsStatistics: function() {
            $('#loading_wrap').fadeIn();
            data = $('#findings-form').serialize();
            $.post(api_base_route + '/findings/statistics', data, function (stats) {
                if(stats.error) {
                    alert('No existen hallazgos reportados para la sucursal seleccionada');
                } else {
                    $('#graph-modal').modal();

//------------------- Charts ------------------------------------------------------------------------------------------
                    //Adding content to the modal
                    switch (stats.graph_type) {
                        case "1":
                            //Graphic number 1 ---- Findings
                                $("#graph1").siblings().fadeOut();
                                $("#graph1").fadeIn();
                                var ctx = $("#graph1");
                                var graph1 = new Chart(ctx, {
                                    type: 'bar',
                                    data: {
                                        labels: ["Detectados", "Corregidos"],
                                        datasets: [{
                                            label: '# de Reportes',
                                            data: [stats.reported_findings, stats.corrected_findings],
                                            backgroundColor: [
                                                '#376f9a',
                                                '#91b13a'
                                            ],
                                            borderColor: [
                                                '#000000',
                                                '#000000'
                                            ],
                                            borderWidth: 1
                                        }]
                                    },
                                    options: {
                                        scales: {
                                            yAxes: [{
                                                ticks: {
                                                    beginAtZero: true
                                                }
                                            }]
                                        }
                                    }
                                });
                        break;

                        case "2":
                            //Graphic number 2 ---- Findings
                            $("#graph2").siblings().fadeOut();
                            $("#graph2").fadeIn();

                            $("#graph2").html("<h3>Porcentaje de Hallazgos corregidos: " + stats.corrected_porcent + "%</h3>");
                        break;

                        case "3":
                            $("#graph3").siblings().fadeOut();
                            $("#graph3").fadeIn();
                            var ctx = $("#graph3");
                            var graph3 = new Chart(ctx,{
                                type: 'doughnut',
                                data: {
                                    labels: ["Pendientes", "En Proceso", "Corregidos"],
                                    datasets: [{
                                        label: '# de Reportes',
                                        data: [stats.pendent_findings, stats.progress_findings, stats.completed_findings],
                                        backgroundColor: [
                                            '#e74c3c',
                                            '#e7cd3c',
                                            '#91b13a'
                                        ],
                                        borderColor: [
                                            '#000000',
                                            '#000000',
                                            '#000000'
                                        ],
                                        borderWidth: 1
                                    }]
                                },
                            });
                        break;

                        case "4":
                            $("#graph4").siblings().fadeOut();
                            $("#graph4").fadeIn();
                            var ctx = $("#graph4");
                            var graph4 = new Chart(ctx, {
                                type: 'bar',
                                data: {
                                    labels: ["Leve", "Moderado", "Alto", "Intolerable"],
                                    datasets: [{
                                        label: '# de Reportes',
                                        data: [stats.low_risk_findings, stats.moderate_risk_findings, stats.high_risk_findings, stats.intolerable_risk_findings],
                                        backgroundColor: [
                                            '#bbbbbb',
                                            '#27acc9',
                                            '#fae580',
                                            '#c93727'
                                        ],
                                        borderWidth: 1
                                    }]
                                },
                                options: {
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero: true
                                            }
                                        }]
                                    }
                                }
                            });
                        break;

						case "5":
							$("#graph5").siblings().fadeOut();
							$("#graph5").fadeIn();
							var ctx = $("#graph5");
							var graph5 = new Chart(ctx, {
								type: 'bar',
								data: {
									labels: stats.types,
									datasets: [{
										label: '# de Reportes',
										data: stats.findings_qty,
										backgroundColor: '#376f9a',
										borderWidth: 1
									}]
								},
								options: {
									scales: {
										yAxes: [{
											ticks: {
												beginAtZero: true
											}
										}]
									}
								}
							});
						break;

						case "6":
							$("#graph6").siblings().fadeOut();
							$("#graph6").fadeIn();
							var ctx = $("#graph6");
							var graph6 = new Chart(ctx, {
								type: 'bar',
								data: {
									labels: stats.reporters,
									datasets: [{
										label: '# de Reportes',
										data: stats.findings_qty,
										backgroundColor: '#376f9a',
										borderWidth: 1
									}]
								},
								options: {
									scales: {
										yAxes: [{
											ticks: {
												beginAtZero: true
											}
										}]
									}
								}
							});
						break;

						case "7":
							$("#graph7").siblings().fadeOut();
							$("#graph7").fadeIn();
							var ctx = $("#graph7");
							var graph6 = new Chart(ctx, {
								type: 'bar',
								data: {
									labels: stats.areas,
									datasets: [{
										label: '# de Reportes',
										data: stats.findings_qty,
										backgroundColor: '#376f9a',
										borderWidth: 1
									}]
								},
								options: {
									scales: {
										yAxes: [{
											ticks: {
												beginAtZero: true
											}
										}]
									}
								}
							});
						break;
                    }
                }

                $('#loading_wrap').fadeOut();
            }.bind(this)).fail(function(e) {
                $('#loading_wrap').fadeOut();
                alert('Error 0x0001: Error interno del servidor');
            });
        }

    },

    computed: {
        businessLogoFolder: function() {
            return public_base_route + '/img/content-images/businesses/logos/';
        }
    }
});


//Vue instance always on the bottom of script
new Vue({

	el: 'body',
	data: {
	    businesses: [],
		headquarters: [],
		blocks: [],
		departments: [],
		areas: [],
		added_areas: [],
		places: [],

		active_block: 0,
		active_department: 0,
		active_area: 0,
		active_place: 0,

		employee: [],
		tasks: [],
	},

	methods: {
		addTask: function(plan_id) {
			task_form= $('#task-form');
			data= task_form.serialize();

			//Fields variables
			task_title= $('#task-title').val();
			task_body= $('#task-body').val();
			task_solution_date= $('#task-solution-date').val();
			employee_identity= $('#employee-identity').val();
			//Check fields
			if(task_title.length > 0 && task_body.length > 0 && task_solution_date.length > 0 && employee_identity > 0) {
				$.post(api_base_route+'/reports/tasks/store', data, function(task) {
					this.tasks.push(task);
					$('#add-task').slideUp();

					//Empty all fields
					$('#task-title').val('');
					$('#task-body').val('');
					$('#task-solution-date').val('');
					$('#employee-identity').val('');
					$('#employee-data').slideUp();
				}.bind(this));
			} else {
				alert('Debe proporcionar todos los datos de la tarea');
			}
		}
	},
});