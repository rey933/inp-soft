<?php
/**
 * Created by Carlos José Rey Sayago.
 * User: crey
 * Date: 19/09/2016
 * Time: 08:53 AM
 */

namespace App\IS\Repositories\Areas;


use App\IS\Repositories\Base\BaseRepo;

class AreaRepo extends BaseRepo
{

    public function getModel()
    {
        return new Area();
    }
}