<?php

namespace App\IS\Repositories\INP;

use Illuminate\Database\Eloquent\Model;

class InjureType extends Model
{
    protected $table= 'inpsasel-injuries';
}
