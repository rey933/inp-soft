<?php

use Illuminate\Database\Seeder;

class FindingTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('finding_types')->insert([
            'title'=> 'Plan de Inspección'
        ]);

        DB::table('finding_types')->insert([
            'title'=> 'Inspecciones Externas'
        ]);

        DB::table('finding_types')->insert([
            'title'=> 'Ordenamientos INPSASEL'
        ]);

        DB::table('finding_types')->insert([
            'title'=> 'Inspección Organismos del Estado'
        ]);

        DB::table('finding_types')->insert([
            'title'=> 'Comité SSL'
        ]);

        DB::table('finding_types')->insert([
            'title'=> 'Delegado de Prevención'
        ]);

        DB::table('finding_types')->insert([
            'title'=> 'Investigación / Reporte AT'
        ]);

        DB::table('finding_types')->insert([
            'title'=> 'Reporte de Incidente'
        ]);

        DB::table('finding_types')->insert([
            'title'=> 'Reporte de Evento'
        ]);

        DB::table('finding_types')->insert([
            'title'=> 'Auditorías Externas'
        ]);

        DB::table('finding_types')->insert([
            'title'=> 'Auditorías Internas'
        ]);

        DB::table('finding_types')->insert([
            'title'=> 'Visitas Coorporativas PLUMROSE'
        ]);

        DB::table('finding_types')->insert([
            'title'=> 'Programa Base de Higiene Ocupacional'
        ]);
    }
}
