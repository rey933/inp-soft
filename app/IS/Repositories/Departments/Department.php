<?php

namespace App\IS\Repositories\Departments;

use App\IS\Repositories\Base\BaseModel;
use Illuminate\Database\Eloquent\Model;

class Department extends BaseModel
{
    protected $table= 'departments';
    protected $fillable= [
    	'block_id', 'name', 'description',
    ];

    public function areas()
    {
    	return $this->belongsToMany('App\IS\Repositories\Areas\Area', 'area_department', 'department_id', 'area_id');
    }
}
