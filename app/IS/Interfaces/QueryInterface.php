<?php

namespace App\IS\Interfaces;


interface QueryInterface
{
    public function all(); //Get all model records
    public function search($id); //Search an id into the model
    public function remove($id); //Remove a record into the model
    public function update($id, $data); //Update a record into the model
}