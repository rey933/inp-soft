@extends('layouts.master')
    @section('post_style')
        <style>
            .image {
                background: url("{!! asset('img/content-images/findings/reportes.jpg') !!}") no-repeat;
                background-size: 100%;
                height: 70px;
                margin: 0 auto;
            }
        </style>
    @stop
    @section('content_title')
        Mis Reportes de Hallazgos
    @stop

    @section('bread')
        <li><a href="{!! url('system/dashboard') !!}">Dashboard</a></li>
        <li class="active">Mis Reportes de Hallazgos</li>
    @stop

    @section('content')
        <div class="image"></div>
        @if($findings->count() > 0)
            <table class="table table-striped">
                <thead>
                <th>Fecha de Reporte</th>
                <th>Planteamiento</th>
                </thead>
                <tbody>
                @foreach($findings as $finding)
                    <tr>
                        <td>{!! $finding->created_at !!}</td>
                        <td>{!! $finding->statement !!}</td>
                        <td>
                            <a href="{!! url('system/reports/findings/'.$finding->id) !!}" class="btn btn-default btn-sm change-status-btn"
                               title="Ver Datos Completos">
                                <i class="fa fa-eye"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {!! $findings->links() !!}

        @else
            No se han encontrado Reportes
        @endif
    @stop