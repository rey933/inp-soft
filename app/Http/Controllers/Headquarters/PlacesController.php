<?php

namespace App\Http\Controllers\Headquarters;

use Illuminate\Http\Request;

use App\IS\Repositories\Places\PlaceRepo;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PlacesController extends Controller
{
	protected $place;
    
    public function __construct(PlaceRepo $place)
    {
    	$this->place= $place;
    }

    public function search($area_id)
    {
    	$places= $this->place->getModel()->where('area_id', '=', $area_id)->get()->toJson();
    	return $places;
    }
}
