<?php

namespace App\IS\Repositories\Employees;

use App\IS\Repositories\Base\BaseModel;
use App\IS\Repositories\Reports\Finding;

class Employee extends BaseModel
{
    protected $table= 'employees';
    protected $fillable= [
        'last_name', 'name', 'identity', 'birth_date', 'admission_date', 'position', 'supervisor_identity'
    ];

    public function findings()
    {
        return $this->hasMany(Finding::class, 'reporter', 'identity');
    }
}
