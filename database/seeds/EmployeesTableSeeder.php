<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            'last_name' => 'REY SAYAGO',
            'name' => 'CARLOS JOSÉ',
            'identity' => '22286953',
            'birth_date' => '1994-06-28',
            'admission_date' => '2016-08-28',
            'position' => 'Pasante',
            'supervisor_identity' => '12345678',
        ]);
    }
}
