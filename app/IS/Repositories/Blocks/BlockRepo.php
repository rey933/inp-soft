<?php
/**
 * Created by Carlos José Rey Sayago.
 * User: crey
 * Date: 19/09/2016
 * Time: 08:55 AM
 */

namespace App\IS\Repositories\Blocks;


use App\IS\Repositories\Base\BaseRepo;

class BlockRepo extends BaseRepo
{

    public function getModel()
    {
        return new Block();
    }

    //---Getters
    public function name()
    {
    	$name= $this->getModel()->name();
    	return $name;
    }
}