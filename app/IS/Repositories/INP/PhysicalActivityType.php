<?php

namespace App\IS\Repositories\INP;

use Illuminate\Database\Eloquent\Model;

class PhysicalActivityType extends Model
{
    protected $table= 'inpsasel-physical-activities-type';
}
