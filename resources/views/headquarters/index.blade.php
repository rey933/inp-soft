@extends('layouts.master')
    @section('content_title')
        Sucursales de la empresa: {!! $business->name !!}
    @stop

    @section('bread')
        <li><a href="{!! url('system/dashboard') !!}">Dashboard</a></li>
        <li><a href="{!! url('system/businesses/') !!}">Empresas</a></li>
        <li class="active">Sucursales</li>
    @stop

    @section('content')
        @if($business->headquarters()->count() > 0)
            <div class="row">
                <table class="table table-striped">
                    <thead>
                        <th></th>
                        <th>Sucursal</th>
                        <th>Descripción</th>
                        <th>Dirección</th>
                        <th>Acciones</th>
                    </thead>
                    @foreach($business->headquarters()->get() as $headquarter)
                        <tr>
                            <td><img src="{!! asset('img/content-images/businesses/logos/'.$business->image) !!}"
                                     alt=""
                                     style="width: 50px">
                            </td>
                            <td>{!! $headquarter->name !!}</td>
                            <td>{!! $headquarter->description !!}</td>
                            <td>{!! $headquarter->direction !!}</td>
                            <td>
                                <a href="{!! action('Headquarters\HeadquarterController@spaceManagement', [
                                                    'id'=> $headquarter->id]) !!}"
                                   class="btn btn-default btn-sm"
                                   title="Administrador de distribución"><i class="fa fa-cubes"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <a href="{!! action('Businesses\BusinessController@index') !!}"
               class="btn btn-default btn-sm">
                Volver al listado de Empresas
            </a>
            <a href="{!! action('Headquarters\HeadquarterController@create', ['id'=> $business->id]) !!}"
               class="btn btn-primary btn-sm">
                Agregar una nueva Sucursal
            </a>
        @else
            <div class="alert alert-danger">
                <strong>Aún no ha sido añadida ninguna Sucursal para esta Empresa</strong>
                <p>
                    Si desea acceder a la creación de reportes, debe crear por lo menos una.
                </p>
                <br>
                <a href="{!! action('Headquarters\HeadquarterController@create', ['id'=> $business->id]) !!}" class="btn btn-success btn-sm" title="Crear Sucursal"><i class="fa fa-pencil"></i></a>
                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-user-md" title="Contactar a Soporte Técnico"></i></a>
            </div>
        @endif
    @stop