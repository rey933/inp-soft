@extends('layouts.master')
    @if($businesses->count() > 0)
    @section('post_style')
        {!! Html::style('css/wizard.css') !!}
        <style>
            .wizard > .steps .current a,
            .wizard > .steps .current a:hover,
            .wizard > .steps .current a:active
            {
                background: #be603f;
                color: #fff;
                cursor: default;
            }

            .wizard > .steps .done a,
            .wizard > .steps .done a:hover,
            .wizard > .steps .done a:active
            {
                background: #4d7645;
                color: #fff;
            }

            .wizard > .actions a,
            .wizard > .actions a:hover,
            .wizard > .actions a:active
            {
                background: #be603f;
                color: #fff;
                display: block;
                padding: 0.5em 1em;
                text-decoration: none;

                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
            }
        </style>
    @stop
    @section('content_title')
        Reporte de Hallazgo
    @stop

    @section('bread')
        <li><a href="{!! url('system/dashboard') !!}">Dashboard</a></li>
        <li><a href="{!! url('system/reports/') !!}">Reportes</a></li>
        <li class="active">Reporte de Hallazgo</li>
    @stop

    @section('content')
        {!! Form::open(['name'=> 'report', 'id'=> 'report-form', 'action'=> 'Reports\FindingsController@store']) !!}
        <div id="wizard">
            <h1>Descripción del Hallazgo </h1>

            <div>
                <div class="form-group">
                    {!! Form::label('finding_statement', 'Planteamiento') !!}
                    {!! Form::text('finding_statement', NULL, ['class'=> 'form-control required',
                                                           'placeholder'=> 'Proporcione un planteamiento para el Hallazgo']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('finding_type', 'Tipo') !!}
                    {!! Form::select('finding_type', $types, NULL, ['class'=> 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('finding_risk_level', 'Nivel de Riesgo') !!}
                    {!! Form::select('finding_risk_level', ['Leve'=> 'Leve', 'Moderado'=> 'Moderado', 'Alto'=> 'Alto', 'Intolerable'=> 'Intolerable'], NULL, ['class'=> 'form-control required']) !!}
                </div>
                <p>
                    Los planteamientos son la cabecera del hallazgo. Lo conforma una frase breve lo suficientemente descriptiva como para transmitir la idea del problema.
                </p>
            </div>

            <h1>Ubicación del Hallazgo</h1>
            <div>
                <div class="form-group">
                    <is-businesses-select list=""
                                          :headquarters.sync="headquarters">                        
                    </is-businesses-select>
                </div>
                <div class="form-group" 
                     id="headquarter"
                     style="display: none;">
                    <is-headquarters-select :list="headquarters"
                                            :blocks.sync="blocks">                       
                    </is-headquarters-select>
                </div>
                <div class="form-group" 
                     id="block"
                     style="display: none;">
                    <is-blocks-select :list="blocks"
                                      :departments.sync="departments">                        
                    </is-blocks-select>
                </div>
                <div class="form-group" 
                     id="department"
                     style="display: none;">
                    <is-departments-select :list="departments"
                                           :areas.sync="areas">
                        
                    </is-departments-select>
                </div>
                <div class="form-group" 
                     id="area"
                     style="display: none;">
                    <is-areas-select :list="areas"> </is-areas-select>
                </div>
                <div class="form-group" 
                     id="place"
                     style="display: none;">
                     {!! Form::label('place', 'Lugar del Hallazgo') !!}
                     {!! Form::text('place', NULL, ['id'=> 'place-field', 'class'=> 'form-control required']) !!}
                </div>
            </div>
        </div>

        <!--Components Templates-->
        <template id="is-businesses-select-template">
            {!! Form::label('business', 'Empresa del hallazgo') !!}
            <select name="business_id"
                    class="form-control"
                    id="business"
                    @change="searchHeadquarters()">
                <option selected disabled>
                    ------ Elige una empresa de la lista ------
                </option>
                <option v-for="business in list" 
                        value="@{{business.id}}">
                    @{{business.name}}            
                </option>
            </select>
        </template> <!--#is-businesses-select-template-->

        <template id="is-headquarters-select-template">
            {!! Form::label('headquarters', 'Sucursal del hallazgo') !!}
            <select name="headquarter_id"
                    class="form-control col col-md-10"
                    id="headquarter_id"
                    @change="searchBlocks()">
                <option selected disabled>
                    ------- Elige una sucursal de la lista ------
                </option>
                <option v-for="headquarter in list" 
                        value="@{{headquarter.id}}">
                    @{{headquarter.name}}            
                </option>
            </select>
        </template> <!--#is-headquarters-select-template-->

        <template id="is-blocks-select-template">
            {!! Form::label('blocks', 'Bloque') !!}
            <select name="block_id"
                    class="form-control"
                    id="block_id"
                    v-show="list.length"
                    @change="searchDepartments()">
                <option selected disabled>
                    ------- Elige un bloque de la lista ------
                </option>
                <option v-for="block in list" 
                        value="@{{block.id}}">
                    @{{block.name}}            
                </option>
            </select>            
            <p class="error"
               v-else>
               Error 1x0003: La sucursal seleccionada no posee Bloques asociados. Por favor contacte al Administrador del sistema. <a href="#">Contactar</a>                
            </p>
        </template> <!--#is-blocks-select-template-->

        <template id="is-departments-select-template">
            {!! Form::label('departments', 'Departamento') !!}
            <select name="department_id"
                    class="form-control"
                    id="department_id"
                    v-show="list.length"
                    @change="searchAreas()">
                <option selected disabled>
                    ------- Elige un departamento de la lista ------
                </option>
                <option v-for="department in list" 
                        value="@{{department.id}}">
                    @{{department.name}}            
                </option>
            </select>
            <p class="error"
               v-else>
               Error 1x0004: El Bloque seleccionado no posee Departamentos asociados. Por favor contacte al Administrador del sistema. <a href="#">Contactar</a>                
            </p>
        </template> <!--#is-departments-select-template-->

        <template id="is-areas-select-template">
            {!! Form::label('areas', 'Área de Hallazgo') !!}
            <select name="area_id"
                    class="form-control"
                    id="area_id"
                    v-show="list.length"
                    @change="showPlace()">
                <option selected disabled>
                    ------- Elige un departamento de la lista ------
                </option>
                <option v-for="area in list" 
                        value="@{{area.id}}">
                    @{{area.name}}            
                </option>
            </select>
            <p class="error"
               v-else>
               Error 1x0005: El Departamento seleccionado no posee Áreas asociadas. Por favor contacte al Administrador del sistema. <a href="#">Contactar</a>
            </p>
        </template> <!--#is-areas-select-template-->

        <template id="is-employees-search-template">
            {!! Form::open(['class'=> 'form-inline', 
                            'onsubmit'=> 'return false']) !!}
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-child"></i></div>
                        {!! Form::text('employee', NULL, ['id'=> 'employee-identity',
                                                          'class'=> 'form-control', 
                                                          'placeholder'=> 'Cédula: 12478526',]) !!}
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-default btn-sm"
                            @click="searchEmployee()">
                            <i class="fa fa-search"></i>
                    </button>
                </div>
                
            {!! Form::close() !!}
            <br>
            <div id="employee-data"
                 class="well" 
                 style="display:none;"
                 role="alert"><strong>Nombre:</strong><span class="full-name"></span>
            </div>
            <div id="employee-error"
                 class="alert alert-danger" 
                 style="display: none;" 
                 role="alert">
                 <strong>Error:</strong> no se ha encontrado al empleado especificado
            </div>
        </template> <!--#is-employees-search-->

        {!! Form::close() !!}


    @stop

    @section('post_script')
        <script>           
           var api_base_route= "{!! url('api') !!}";
        </script>
        {!! Html::script('js/jquery.steps.js') !!}
        {!! Html::script('js/localization/messages_es.js') !!}
        {!! Html::script('js/date/bootstrap-datepicker.js') !!}
        {!! Html::script('js/date/bootstrap-datepicker.es.min.js') !!}
        <script>
            var form= $('#report-form');
            var wizard= $('#wizard').steps({
                labels: {
                    cancel: 'Cancelar',
                    current: 'Actual',
                    next: 'Siguiente',
                    previous: 'Anterior',
                    loading: 'Cargando',
                    finish: 'Finalizar',
                },
                transitionEffect: "slideLeft",
                stepsOrientation: 1 ,
                onStepChanging: function(event, currentIndex, newIndex) {
                    // Allways allow previous action even if the current form is not valid!
                    if (currentIndex > newIndex) {
                        return true;
                    }
                    if(currentIndex==1) {
                        var place= $('#place-field').val();
                        if(place == '') {
                            alert('Debe Especificar la ubicación del Hallazgo');
                            return false;
                        }
                    }
                    // Needed in some cases if the user went back (clean up)
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        form.find(".body:eq(" + newIndex + ") label.error").remove();
                        form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                    }
                    form.validate().settings.ignore = ":disabled,:hidden";
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    form.removeAttr('onsubmit');
                    form.submit();
                }
            });

            $('.input-group.date').datepicker({
                format: 'yyyy-mm-dd',
                language: 'es',
                startDate: 'today'
            });
        </script>
    @stop
    @else
        @section('content_title')
            Reporte de Hallazgo
        @stop

        @section('bread')
            <li><a href="{!! url('system/dashboard') !!}">Dashboard</a></li>
            <li><a href="{!! url('system/reports/') !!}">Reportes</a></li>
            <li class="active">Reporte de Hallazgo</li>
        @stop
        @section('content')
        <div class="alert alert-danger">
            <strong>No ha sido registrada aún ninguna empresa</strong>
            <p>
                Si desea acceder a la creación de reportes, visualización de estadísticas y comprobación de resultados.
                Debe por lo menos crear una Empresa.
            </p>
            <br>
            <a href="{!! url('system/businesses/create') !!}" class="btn btn-success btn-sm" title="Crear Empresa"><i class="fa fa-pencil"></i></a>
            <a href="#" class="btn btn-default btn-sm"><i class="fa fa-user-md" title="Contactar a Soporte Técnico"></i></a>
        </div>
        @stop
    @endif