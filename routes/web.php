<?php

use App\Mail\TestMail;
use Illuminate\Support\Facades\Mail;

//System routes

Route::group(['prefix' => '/'], function () {
    Route::get('/email-test', function() {
        return Mail::to('crey@plumrose.com')->send(new TestMail());
    });

    Route::get('/', function() {
       return redirect()->to('login');
       //return view('layouts.master');
    });
    Route::get('login', function () {
        return view('auth/login');
    })->middleware('guest');
        Route::post('login', 'Auth\AuthController@login');
    Route::get('logout', 'Auth\AuthController@logout');

    Route::group(['prefix' => 'system', 'middleware' => 'auth'], function() {
        Route::get('/', function() {
            return redirect()->to('system/dashboard');
        });      

        //Dashboard
        Route::get('dashboard', function() {
            return view('dashboard');
        });

        //Businesses section
        Route::get('businesses/statistics', 'Businesses\BusinessController@statistics', ['middleware'=> 'role.manager']);
        Route::resource('businesses', 'Businesses\BusinessController', ['middleware'=> 'role.manager']);

        //Headquarters section
        Route::get('headquarters/{id}', ['as' => 'headquarters.index', 'uses' => 'Headquarters\HeadquarterController@index']);
        Route::get('headquarters/create/{id}', ['as' => 'headquarters.create', 'uses' => 'Headquarters\HeadquarterController@create']);
        Route::post('headquarters/store/{id}', ['as' => 'headquarters.store', 'uses' => 'Headquarters\HeadquarterController@store']);
        Route::get('headquarters/management/{id}', ['as' => 'headquarters.management', 'uses' => 'Headquarters\HeadquarterController@spaceManagement']);
        Route::resource('headquarters', 'Headquarters\HeadquarterController', ['middleware'=> 'role.manager',
                                                                               'except'=> ['index', 'create', 'store']]);
        //Route::resource('blocks', 'Headquarters\BlocksController', ['middleware'=> 'role.manager']);
        Route::resource('departments', 'Headquarters\DepartmentsController', ['middleware'=> 'role.manager']);

        //Reports section
        Route::group(['prefix'=> 'reports'], function() {
            Route::resource('findings', 'Reports\FindingsController');
            Route::resource('accidents', 'Reports\AccidentsController');
        });

        //User section
        Route::group(['prefix'=> 'user'], function() {
            Route::group(['prefix' => 'register'], function() {
                Route::get('/', 'Auth\RegisterController@registerAllTransaction')->middleware('role.admin');
                    Route::post('/', 'Auth\RegisterController@register')->middleware('role.admin');

                Route::get('/manager', 'Auth\RegisterController@registerManagerTransaction')->middleware('role.manager');
                    Route::post('/manager', 'Auth\RegisterController@register')->middleware('role.manager');
            });
        });
    });
});
