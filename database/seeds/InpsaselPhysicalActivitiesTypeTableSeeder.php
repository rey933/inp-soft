<?php

use Illuminate\Database\Seeder;

class InpsaselPhysicalActivitiesTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inpsasel-physical-activities-type')->insert([
            'title'=> 'OPERACIONES CON MÁQUINAS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-physical-activities-type')->insert([
            'title'=> 'TRABAJOS CON HERRAMIENTAS MANUALES',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-physical-activities-type')->insert([
            'title'=> 'CONDUCIR/ESTAR A BORDO DE UN MEDIO DE TRANSPORTE - EQUIPO DE CARGA',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-physical-activities-type')->insert([
            'title'=> 'MANIPULACION DE OBJETOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-physical-activities-type')->insert([
            'title'=> 'TRANSPORTE MANUAL',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-physical-activities-type')->insert([
            'title'=> 'MOVIMIENTO',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-physical-activities-type')->insert([
            'title'=> 'ESTAR PRESENTE',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-physical-activities-type')->insert([
            'title'=> 'OTRA ACTIVIDAD FÍSICA ESPECIFICADA NO CODIFICADA EN ESTA CLASIFICACIÓN',
            'inpsasel-code'=> 1,
        ]);
    }
}
