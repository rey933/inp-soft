<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InpsaselPhysicalActivitiesType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inpsasel-physical-activities-type', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('inpsasel-code');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inpsasel-physical-activities-type');
    }
}
