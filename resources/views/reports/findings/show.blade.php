@extends('layouts.master')
	@section('post_style')
		{!! Html::style('css/slider/jquery.bxslider.css') !!}
		{!! Html::style('unitegallery/css/unite-gallery.css') !!}
		{!! Html::style('unitegallery/themes/default/ug-theme-default.css') !!}
		{!! Html::style('css/date/bootstrap-datepicker.css') !!}
		<style type="text/css">
			.cover-image{
				background-size: cover !important;
				height: 225px !important;
			}
		</style>
	@stop
	@section('bread')
        <li><a href="{!! url('system/dashboard') !!}">Dashboard</a></li>
        <li><a href="{!! url('system/reports/findings/') !!}">Mis Reportes de Hallazgos</a></li>
        <li class="active">Reporte de Hallazgo</li>
    @stop

	@section('content_title', 'N-'. $finding->id .': '. $finding->statement)
	@section('content')
		<nav class="options">
			<ul class="nav nav-tabs nav-justified">
			  <li><a href="#!" class="report-option">Reporte</a></li>
			  <li><a href="#!" class="plan-option">Plan de Acción 
			  	@if($finding_repo->getFindingPlan($finding->id)->status == 'Planning')
					<span> class="label label-info">En Planificación</span></a>
				@elseif($finding_repo->getFindingPlan($finding->id)->status == 'Creating')
					<span class="label label-warning">En Construcción</span></a>
				@else
					<span class="label label-sucesss">Completado</span></a>
				@endif			  	
			  </li>
			  <li><a href="#!" class="gallery-option">Galería</a></li>
			</ul>
		</nav>

		<div id="report-details" style="display: block;">			
			<pre>
				<b>Planteamiento del Hallazgo</b>: {!! $finding->statement !!}
				<b>Fecha y Hora de Reporte</b>: {!! $finding->created_at !!}
				<b>Reportado por</b>: {!! $finding_repo->getReporterFullName($finding->id) !!}
				<b>Lugar de hallazgo</b>: 
				<p>
					Empresa: {!! $finding_repo->getFindingBusiness($finding->id) !!}
					Sede: {!! $finding_repo->getFindingHeadquarter($finding->id) !!}
					Bloque: {!! $finding_repo->getFindingBlock($finding->id) !!}
					Departamento: {!! $finding_repo->getFindingDepartment($finding->id) !!}
					Lugar: {!! $finding_repo->getFindingPlace($finding->id) !!}
				</p>
			</pre>
		</div> <!-- /#report-details-->

		<div id="plan" style="display: none;">
			<div class="well">
				@if($finding_repo->getFindingPlan($finding->id)->status == 'Planning')
					El Plan de Acción se encuentra actualmente en etapa de discusión. Misma que está ocurriendo en la sala <a href="#">N-9874</a>
				@elseif($finding_repo->getFindingPlan($finding->id)->status == 'Creating')
					<h3>Plan de Acción:</h3>
					<div>
						<is-action-plan :list.sync="tasks"
										plan_id="{!! $finding_repo->getFindingPlan($finding->id)->id !!}">
											
						</is-action-plan>
					</div>					
					<button id="add-task-btn" class="btn btn-success btn-sm" title="Agregar Tarea"><i class="fa fa-plus"></i></button>
					<button class="btn btn-default btn-sm" title="Contactar a Soporte técnico"><i class="fa fa-support"></i></button>

					<div id="add-task"
						 style="padding-top: 20px; display: none;">
						<h4>Datos de la tarea:</h4>
						{!! Form::open(['method'=> 'POST', 'id'=> 'task-form']) !!}
							<div class="form-group">
								{!! Form::label('task_title', 'Tarea') !!}
								{!! Form::text('task_title', NULL, ['id'=> 'task-title',
																	'class'=> 'form-control',
																	'placeholder'=> 'Ejemplo: Corregir fallos en clasificación de documentos.']) !!}
							</div>
							<div class="form-group">
								{!! Form::label('task_solution_date', 'Fecha de finalización propuesta') !!}
								<div class="input-group date">									
									{!! Form::text('task_solution_date', NULL, ['id'=> 'task-solution-date',
																		'class'=> 'form-control',
																		'readonly']) !!}
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
							<div class="form-group">
								{!! Form::label('task_title', 'Descripción de la tarea') !!}
								{!! Form::textarea('task_body', NULL, ['id'=> 'task-body',
																	'class'=> 'form-control',
																	'placeholder'=> 'Ejemplo: Los documentos tienen una organización prioritaria errónea según la norma ISO-9001']) !!}
							</div>

							<input type="hidden" name="mandatory_identity" id="mandatory-identity">
							<input type="hidden" name="plan_id" value="{!!$finding_repo->getFindingPlan($finding->id)->id!!}">
						{!! Form::close() !!}
						<br>
						<h4>Responsable:</h4>
						<is-employees-search :list.sync="employee"></is-employees-search>
						<button class="btn btn-success" v-show="employee.length"
														@click="addTask({!! $finding_repo->getFindingPlan($finding->id)->id !!})">Añadir Tarea</button>
					</div>
				@else
					El plan de Acción ha sido realizado en su totalidad
				@endif				
			</div>
		</div> <!-- /#plan-->

		<div id="gallery-container" style="display: none;">
			<div id="gallery">		
				<img alt="GitHub" src="http://unitegallery.net/newimages/image53.jpg"
					data-image="http://unitegallery.net/newimages/image53.jpg"
					data-description="GitHub es una plataforma para el control de versiones a través de internet.">
				<img alt="GitHub" src="http://unitegallery.net/newimages/image50.jpg"
					data-image="http://unitegallery.net/newimages/image50.jpg"
					data-description="GitHub es una plataforma para el control de versiones a través de internet.">
				<img alt="GitHub" src="http://unitegallery.net/newimages/image45.jpg"
					data-image="http://unitegallery.net/newimages/image45.jpg"
					data-description="GitHub es una plataforma para el control de versiones a través de internet.">
				<img alt="GitHub" src="http://unitegallery.net/newimages/image76.jpg"
					data-image="http://unitegallery.net/newimages/image76.jpg"
					data-description="GitHub es una plataforma para el control de versiones a través de internet.">
				<img alt="GitHub" src="http://unitegallery.net/newimages/image69.jpg"
					data-image="http://unitegallery.net/newimages/image69.jpg"
					data-description="GitHub es una plataforma para el control de versiones a través de internet.">
			</div> <!-- / #gallery-->
		</div>

		<template id="is-employees-search-template">
			{!! Form::open(['onsubmit'=> 'return false', 'class'=> 'form-inline']) !!}
				{!! Form::label('employee_identity', 'Buscar empleado por cédula') !!}
				{!! Form::text('employee_identity', NULL, ['id'=> 'employee-identity', 'class'=> 'form-control',
														   'placeholder'=> 'Ejemplo: 14785963']) !!}
				<button class="btn btn-default btn-sm"
						@click="searchEmployee()">
					<i class="fa fa-search"></i>
				</button>
			{!! Form::close() !!}

			<br>

			<div id="employee-error" style="display: none" class="alert alert-danger">
				Empleado no encontrado
			</div>

			<div id="employee-data" style="display: none">
				<div class="alert alert-info">
					<b>Nombre del Empleado a asignar la tarea:</b><span class="full-name"></span>
				</div>
			</div>
		</template>

		<template id="is-action-plan-template">
			<table class="table table-hover" v-show="list.length">
							<thead>
								<th>#</th>
								<th>Tarea</th>
								<th>Descripción</th>
								<th>Fecha propuesta</th>
								<th>Estado</th>
								<th>Acciones</th>
							</thead>
							<tr v-for="task in list" :class="{ 'danger': task.status == 'Pendent', 
															   'warning': task.status == 'In Progress',
															   'success': task.status == 'Finished',}">
								<td></td>
								<td>@{{ task.title }}</td>
								<td>@{{ task.body }}</td>
								<td>@{{ task.proposed_completion_date }} </td>
								<td>
									<span v-if="task.status== 'Pendent'">Pendiente</span>
									<span v-if="task.status== 'In Progress'">En Progreso</span>
									<span v-if="task.status== 'Finished'">Finalizado</span>
									<select name="task_status" class="task-status"
											@change="changeTaskStatus(task, $event)"
											style="display: none;">
										<option value="Pendent">Pendiente</option>
										<option value="In Progress">En Progreso</option>
										<option value="Finished">Finalizado</option>
									</select>
								</td>
								<td>
									<button class="btn btn-default btn-sm change-status-btn"
											title="Modificar Estado de la Tarea"
											@click="displayStatus($event)">
										<i class="fa fa-pencil"></i>
									</button>
									<button class="btn btn-danger btn-sm change-status-btn"
											title="Borrar Tarea"
											@click="removeTask(task)">
									<i class="fa fa-trash-o"></i>
									</button>
								</td>
							</tr>
			</table>
			<p v-else>Este plan de acción no tiene tareas asociadas</p>
		</template>

	@stop

	@section('post_script')
		<script>           
           var api_base_route= "{!! url('api') !!}";
        </script>
		{!! Html::script('unitegallery/js/unitegallery.min.js') !!}
		{!! Html::script('unitegallery/themes/tiles/ug-theme-tiles.js') !!}
		{!! Html::script('js/date/bootstrap-datepicker.js') !!}
        {!! Html::script('js/date/bootstrap-datepicker.es.min.js') !!}
		<script type="text/javascript">				
				$("#gallery").unitegallery({
					tiles_type:"justified",
					todayHighlight: true,
				});

				$('.input-group.date').datepicker({
					language: 'es',
					format: 'yyyy-mm-dd',
					startDate: 'today',
					todayHighlight: true,
				});

				$('.report-option').click(function() {
					$('#plan').slideUp();
					$('#gallery-container').slideUp();
					$('#report-details').slideToggle();
				});

				$('.plan-option').click(function() {
					$('#report-details').slideUp();
					$('#gallery-container').slideUp();
					$('#plan').slideToggle();
				});

				$('.gallery-option').click(function() {
					$('#report-details').slideUp();
					$('#plan').slideUp();
					$('#gallery-container').slideToggle();
				});

				$('#add-task-btn').click(function() {
					$('#add-task').slideDown();
				});
		</script>
	@stop