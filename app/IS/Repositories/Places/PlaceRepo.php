<?php
/**
 * Created by Carlos José Rey Sayago.
 * User: crey
 * Date: 15/09/2016
 * Time: 07:39 AM
 */

namespace App\IS\Repositories\Places;


use App\IS\Repositories\Base\BaseRepo;

class PlaceRepo extends BaseRepo
{

    public function getModel()
    {
        return new Place();
    }
}