<?php
/**
 * Created by Carlos José Rey Sayago.
 * User: crey
 * Date: 19/09/2016
 * Time: 08:56 AM
 */

namespace App\IS\Repositories\Departments;


use App\IS\Repositories\Base\BaseRepo;

class DepartmentRepo extends BaseRepo
{
    public function getModel()
    {
        return new Department();
    }
}