<?php

use Illuminate\Database\Seeder;

class InpsaselJobsTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inpsasel-jobs-type')->insert([
            'title'=> 'TAREAS DE PRODUCCIÓN, TRANSFORMACIÓN, TRATAMIENTO, ALMACENAMINETO',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-jobs-type')->insert([
            'title'=> 'LABORES DE MOVIMIENTOS DE TIERRAS, CONSTRUCCIÓN, MANTENIMIENTO, DEMOLICIÓN',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-jobs-type')->insert([
            'title'=> 'LABORES DE TIPO AGRÍCOLA, FORESTAL, HORTÍCOLA, PISCICOLA, CON ANIMALES VIVOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-jobs-type')->insert([
            'title'=> 'ACTIVIDADES DE SERVICIO A EMPRESAS O A PERSONAS Y TRABAJOS INTELECTUALES',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-jobs-type')->insert([
            'title'=> 'TRABAJOS RELACIONADOS CON LAS TAREAS CODIFICADAS EN 10, 20, 30 Y 40',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-jobs-type')->insert([
            'title'=> 'CIRCULACIÓN, ACTIVIDADES DEPORTIVAS Y ARTISTICAS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-jobs-type')->insert([
            'title'=> 'ATRAPADO EN, DEBAJO, ENTRE O POR',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-jobs-type')->insert([
            'title'=> 'OTROS TIPO DE TRABAJO NO CODIFICADOS EN ESTA CLASIFICACIÓN',
            'inpsasel-code'=> 1,
        ]);
    }
}
