<?php

use Illuminate\Database\Seeder;

class InpsaselBodyPartsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inpsasel-body-parts')->insert([
            'title'=> 'CABEZA / CARA',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-body-parts')->insert([
            'title'=> 'CUELLO',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-body-parts')->insert([
            'title'=> 'TRONCO',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-body-parts')->insert([
            'title'=> 'ESPALDA',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-body-parts')->insert([
            'title'=> 'EXTREMIDADES SUPERIORES',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-body-parts')->insert([
            'title'=> 'EXTREMIDADES INFERIORES',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-body-parts')->insert([
            'title'=> 'UBICACIONES MÚLTIPLES',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-body-parts')->insert([
            'title'=> 'LESIONES GENERALES',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-body-parts')->insert([
            'title'=> 'PARTES DEL CUERPO NO CLASIFICADAS BAJO OTRO EPÍGRAFE',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-body-parts')->insert([
            'title'=> 'UBICACIONES NO ESPECIFICADAS',
            'inpsasel-code'=> 1,
        ]);
    }
}
