<?php

namespace App\IS\Repositories\Roles;

use App\IS\Repositories\Base\BaseModel;

class Role extends BaseModel
{
    protected $table= 'roles';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user()
    {
        return $this->hasMany('App\User', 'role_id');
    }
}
