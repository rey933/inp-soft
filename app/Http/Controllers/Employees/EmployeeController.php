<?php

namespace App\Http\Controllers\Employees;

use App\IS\Repositories\Employees\EmployeeRepo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EmployeeController extends Controller
{
    protected $employee;

    public function __construct(EmployeeRepo $employee)
    {
        $this->employee= $employee;
    }

    public function search($identity)
    {
    	$employee= $this->employee->getModel()->where('identity', '=', $identity)->get();
    	return $employee;
    }

//    public function test()
//    {
//        $employee= $this->employee_repo->search(1);
//        return $this->employee_repo->getSupervisor($employee);
//    }
}
