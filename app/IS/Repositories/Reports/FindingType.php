<?php

namespace App\IS\Repositories\Reports;

use App\IS\Repositories\Base\BaseModel;
use Illuminate\Database\Eloquent\Model;

class FindingType extends BaseModel
{
    protected $table= 'finding_types';
    protected $fillable= ['title'];

    public function findings()
    {
        return $this->hasMany(Finding::class, 'type_id');
    }
}
