<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Accidents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accidents', function(Blueprint $table) {
            $table->increments('id');

            $table->string('reporter');
            $table->foreign('reporter')
                ->references('identity')
                ->on('employees')
                ->onDelete('cascade');
            $table->string('affected_identity');
            $table->foreign('affected_identity')
                ->references('identity')
                ->on('employees')
                ->onDelete('cascade');
            $table->date('accident_date');
            $table->time('accident_hour');
            $table->enum('accident_classification', ['LESIÓN CPT', 'LESIÓN SPT']);
            $table->enum('accident_category', ['EN PLANTA', 'IN-ITINERE', 'DEPORTIVO', 'BRIGADA']);

            //Ubication
            $table->integer('business_id')->unsigned();
            $table->foreign('business_id')
                ->references('id')
                ->on('businesses')
                ->onDelete('cascade');
            $table->integer('headquarter_id')->unsigned();
            $table->foreign('headquarter_id')
                ->references('id')
                ->on('headquarters')
                ->onDelete('cascade');
            $table->integer('block_id')->unsigned();
            $table->foreign('block_id')
                ->references('id')
                ->on('blocks')
                ->onDelete('cascade');
            $table->integer('department_id')->unsigned();
            $table->foreign('department_id')
                ->references('id')
                ->on('departments')
                ->onDelete('cascade');
            $table->integer('area_id')->unsigned();
            $table->foreign('area_id')
                ->references('id')
                ->on('areas')
                ->onDelete('cascade');
            $table->integer('place_id')->unsigned();
            $table->foreign('place_id')
                ->references('id')
                ->on('places')
                ->onDelete('cascade');

            //Inpsasel section
            $table->integer('place_type_id')->unsigned();
            $table->foreign('place_type_id')
                  ->references('id')
                  ->on('inpsasel-places-type')
                  ->onDelete('cascade');
            $table->string('specific_place_type');

            $table->integer('physical_activity_type_id')->unsigned();
            $table->foreign('physical_activity_type_id')
                ->references('id')
                ->on('inpsasel-physical-activities-type')
                ->onDelete('cascade');
            $table->string('specific_activity_type');

            $table->integer('job_type_id')->unsigned();
            $table->foreign('job_type_id')
                ->references('id')
                ->on('inpsasel-jobs-type')
                ->onDelete('cascade');
            $table->string('specific_job_type');

            $table->integer('material_agent_id')->unsigned();
            $table->foreign('material_agent_id')
                  ->references('id')
                  ->on('inpsasel-material-agents-type')
                  ->onDelete('cascade');
            $table->string('specific_material_agent');

            $table->integer('accident_id')->unsigned();
            $table->foreign('accident_id')
                ->references('id')
                ->on('inpsasel-accidents-type')
                ->onDelete('cascade');
            $table->string('specific_accident');

            $table->integer('body_part_id')->unsigned();
            $table->foreign('body_part_id')
                ->references('id')
                ->on('inpsasel-body-parts')
                ->onDelete('cascade');
            $table->string('specific_body_part');

            $table->integer('injure_id')->unsigned();
            $table->foreign('injure_id')
                ->references('id')
                ->on('inpsasel-injuries')
                ->onDelete('cascade');
            $table->string('specific_injure');

            $table->enum('severity', ['Leve', 'Moderado', 'Grave', 'Muy Grave', 'Mortal']);

            $table->integer('repose_days')->unsigned();
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accidents');
    }
}
