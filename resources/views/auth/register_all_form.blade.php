<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registrar un Usuario</title>
</head>
<body>
    {!! Form::open(['action' => 'Auth\RegisterController@register', 'method' => 'POST']) !!}

        <table>
            <tr>
                <td>Cédula de Identidad del empleado</td>
                <td>
                    <select name="identity_type" id="identity_type">
                        <option value="V-">V-</option>
                        <option value="V-">E-</option>
                    </select>
                    {!! Form::text('identity_number', NULL, ['placeholder' => 'xx.xxx.xxx']) !!}
                </td>
            </tr>
            <tr>
                <td>Nombre de Usuario:</td>
                <td>{!! Form::text('user_name', NULL, ['placeholder' => 'Inserte su nombre de Usuario']) !!}</td>
            </tr>
            <tr>
                <td>Contraseña:</td>
                <td>{!! Form::password('password', []) !!}</td>
            </tr>
            <tr>
                <td>{!! Form::submit('Registrar') !!}</td>
            </tr>
        </table>

    {!! Form::close() !!}

    {!! link_to('system/dashboard', 'Regresar al Dashboard') !!}
</body>
</html>