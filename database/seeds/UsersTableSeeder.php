<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'employee_identity' => '22286953',
            'role_id' => 1,
            'name' => 'cjreysayago',
            'email' => 'crey@plumrose.com',
            'password' => bcrypt('025568'),
        ]);
    }
}
