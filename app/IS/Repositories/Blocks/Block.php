<?php

namespace App\IS\Repositories\Blocks;

use App\IS\Repositories\Base\BaseModel;
use Illuminate\Database\Eloquent\Model;

class Block extends BaseModel
{
    protected $table= 'blocks';

    protected $fillable= [
    	'headquarter_id', 'name', 'description',
    ];

    public function headquarter()
    {
    	return $this->belongsTo('App\IS\Repositories\Headquarter\Headquarter', 'headquarter_id');
    }

    public function departments()
    {
        return $this->hasMany('App\IS\Repositories\Departments\Department', 'block_id');
    }

    public function findings()
    {
        return $this->hasMany('App\IS\Repositories\Reports\Finding', 'block_id');
    }
}
