<?php

namespace App\IS\Repositories\Base;

use App\IS\Interfaces\QueryInterface;

abstract class BaseRepo implements QueryInterface
{
	public abstract function getModel();

    /**
     * @return mixed
     */
	public function all() //Get all model records
	{
		return $this->getModel()->all();
	}

    /**
     * @param $data
     * mixed
     */
	public function create($data) //Create a new record on the model
    {
        $new_record= $this->getModel()->create($data);
        return $new_record;
    }

    /**
     * @param $id
     * @return mixed
     */
	public function search($id) //Search an id into the model
	{
		$collection= $this->getModel()->findOrFail($id);
        if($collection) {
            return $collection;
        } else {
            return response()->json([
               'status'=> 'uncompleted',
               'code'=> '2x0001 - No matches',
            ]);
        }
	}

    /**
     * @param $id
     * @return mixed
     */
	public function remove($id) //Remove a record into the model
	{
	    $record= $this->search($id);

        if(!json_decode($record) && $record->delete()) {
            return response()->json([
               'status'=> 'completed',
            ]);
        } else {
            return response()->json([
                'status'=> 'uncompleted',
                'code'=> '0x0002 - Database management system error. See API docs: Deleting a record'
            ]);
        }
	}

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
	public function update($id, $data) //Update a record into the model
	{
		$record = $this->search($id);
		return $record->save($data);
	}

	public function count() //Return the number of elements in a collection
    {
        return $this->getModel()->count();
    }
}