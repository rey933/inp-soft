<?php

use Illuminate\Database\Seeder;

class InpsaselInjuriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inpsasel-injuries')->insert([
            'title'=> 'HERIDAS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'TRAUMATISMOS SUPERFICIALES',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'CONTUSIONES Y APLASTAMIENTOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'FRACTURAS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'LUXACIONES',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'ESGUINCES, TORCEDURAS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'AMPUTACIÓN Y ENUCLERACIONES',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'TRAUMATISMO INTRACRANEAL',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'LESIONES INTERNAS DEL TORAX, ABDOMEN Y PELVIS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'HERNIA',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'TRAUMATISMO DE LOS NERVIOS Y DE LA MEDULA ESPINAL',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'EFECTO DE CUERPO EXTRAÑO QUE ENTRE POR ORIFICIO',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'QUEMADURAS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'ENVENENAMIENTO POR DROGAS, MEDICAMENTOS Y PRODUCTOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'EFECTOS TOXICOS / INFECC. DE SUSTANC. DE PROCEDENCIA NO MEDICINAL / AG. BIOLOGICOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'EFECTOS NOCIVOS DE LAS RADIACIONES',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'EFECTOS DEL FRIO',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'EFECTOS DEL CALOR',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'EFECTOS DEL RUIDO Y VIBRACION',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'EFECTOS DE LA PRESION ATMOSFERICA',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'EFECTOS DE OTRAS CAUSAS EXTERNAS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'AHOGAMIENTO Y ASFIXIA',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'TRAUMA PSIQUICO, CHOQUE TRAUMATICO',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'REACCIONES ALERGICAS AGUDAS CAUSADAS POR UN AG. DEL MEDIO DE TRABAJO',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'LESIONES MULTIPLES DE NATURALEZA DIFERENTE',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'OTROS EFECTOS ADVERSOS NO CLASIFICADOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-injuries')->insert([
            'title'=> 'TIPO DE LESION DESCONOCIDA O SIN ESPECIFICAR',
            'inpsasel-code'=> 1,
        ]);
    }
}
