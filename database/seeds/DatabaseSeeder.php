<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(EmployeesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(FindingTypesTableSeeder::class);
        $this->call(InpsaselPlacesTypeTableSeeder::class);
        $this->call(InpsaselPhysicalActivitiesTypeTableSeeder::class);
        $this->call(InpsaselJobsTypeTableSeeder::class);
        $this->call(InpsaselMaterialAgentsTypeTableSeeder::class);
        $this->call(InpsaselAccidentsTypeTableSeeder::class);
        $this->call(InpsaselBodyPartsTableSeeder::class);
        $this->call(InpsaselInjuriesTableSeeder::class);
    }
}
