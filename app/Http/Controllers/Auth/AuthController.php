<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;


class AuthController extends Controller
{
    public function login(Request $data)
    {
        if(Auth::attempt(['name'=> $data->username, 'password'=> $data->password])) {
            return redirect()->to('system/dashboard');
        } else {
            return redirect()->to('login')->with(['errors' => ['Usuario o Password incorrectos']]);
        }
    }

    public function isLogged()
    {
        return Auth::check();
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->to('/login');
    }

    public function getUser()
    {
        return Auth::user();
    }
}
