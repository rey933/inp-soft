<?php
/**
 * Created by Carlos José Rey Sayago.
 * User: crey
 * Date: 19/09/2016
 * Time: 08:50 AM
 */

namespace App\IS\Repositories\Businesses;


use App\IS\Repositories\Base\BaseRepo;

class BusinessRepo extends BaseRepo
{
    /**
     * Get the model of the actual Repo
     * @return Business
     */
    public function getModel()
    {
        return new Business();
    }

    /**
     * Get an Array of headquarters
     */
    public function getBusinessHeadquarters()
    {

    }

    /**
     * Get the actual number of Headquarters in a business
     * @return mixed
     */
    public function getBusinessHeadquartersQty()
    {
        return $this->getModel()->headquarter()->count();
    }

    //--Presenters
    /**
     * Print the full business name
     * @return int
     */
    public function displayBusinessName()
    {
        return $this->getModel()->name;
    }

    //Logic Methods

    /**
     * Get the number of Findings in a specified Business
     * @param $business_id
     * @return mixed
     */
    public function numFindings($business_id)
    {
        $business= $this->search($business_id);
        return $business->findings()->count();
    }
}