<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Entrar al sistema</title>
    <!-- Latest compiled and minified CSS - Bootstrap-->
    {!! Html::style('css/bootstrap.min.css') !!}
    <style>
        body {
            background-color: #444;
            background-size: cover;
            background-repeat: no-repeat;
        }
        .form-signin input[type="text"] {
            margin-bottom: 5px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        .form-signin .form-control {
            position: relative;
            font-size: 16px;
            font-family: 'Open Sans', Arial, Helvetica, sans-serif;
            height: auto;
            padding: 10px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        .vertical-offset-100 {
            padding-top: 100px;
        }
        .img-responsive {
            display: block;
            max-width: 100%;
            height: auto;
            margin: auto;
        }
        .panel {
            margin-bottom: 20px;
            background-color: rgba(255, 255, 255, 0.75);
            border: 1px solid transparent;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
            box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row vertical-offset-100">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row-fluid user-row">
                            <img src="{!! asset('img/content-images/banner.png') !!}" class="img-responsive" alt="Manejador de Reportes"/>
                        </div>
                    </div>
                    <div class="panel-body">
                        {!! Form::open(['method' => 'POST', 'action' => 'Auth\AuthController@login']) !!}
                            <fieldset>
                                <label class="panel-login">
                                    <div class="row login_result">
                                        @foreach($errors as $error)
                                            {!! $error !!}
                                        @endforeach
                                    </div>
                                </label>
                                {!! Form::text('username', NULL, ['placeholder' => 'Inserta tu nombre de usuario', 'class' => 'form-control'])!!}
                                {!! Form::password('password', ['class' => 'form-control', 'placeholder'=> 'Inserta tu contraseña'])!!}
                                <br>
                                <br>
                                {!! Form::submit('Login', ['class' => 'btn btn-lg btn-success btn-block']) !!}
                            </fieldset>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Html::script('js/jquery-3.1.0.js') !!}
    {!! Html::script('js/bootstrap.min.js') !!}
    <script>

        var images = ['{!! asset('img/content-images/login-images/bg.jpg') !!}',
                      '{!! asset('img/content-images/login-images/bg2.jpg') !!}',
                      '{!! asset('img/content-images/login-images/bg3.jpg') !!}',
                      '{!! asset('img/content-images/login-images/bg4.jpg') !!}',
                      '{!! asset('img/content-images/login-images/bg5.jpg') !!}',
                      '{!! asset('img/content-images/login-images/bg6.jpg') !!}',
                      '{!! asset('img/content-images/login-images/bg7.jpg') !!}',
                      '{!! asset('img/content-images/login-images/bg8.jpg') !!}',
                      '{!! asset('img/content-images/login-images/bg9.jpg') !!}',
                      '{!! asset('img/content-images/login-images/bg10.jpg') !!}'];

        $('body').css({'background-image': 'url('+images[Math.floor(Math.random() * images.length)] + ')'});

    </script>
</body>
</html>