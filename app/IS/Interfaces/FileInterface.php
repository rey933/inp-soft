<?php
/**
 * Created by Carlos José Rey Sayago
 * User: crey
 * Date: 14/09/2016
 * Time: 03:16 PM
 */

namespace App\IS\Interfaces;


interface FileInterface
{
    public function read();
    public function delete();
    public function upload($rules);
}