<?php

namespace App\Http\Controllers\Headquarters;

use App\IS\Repositories\Businesses\BusinessRepo;
use App\IS\Repositories\Headquarter\HeadquarterRepo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HeadquarterController extends Controller
{
    protected $business;
    protected $headquarter;

    public function __construct(BusinessRepo $business, HeadquarterRepo $headquarter)
    {
        $this->business= $business;
        $this->headquarter= $headquarter;
    }

    protected function headquarterStoreRules()
    {
        return $rules= [
            'headquarter_name'=> 'required|unique:headquarters,name|max:255',
            'headquarter_description'=> 'required|max:255',
            'headquarter_direction'=> 'required|max:255'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $business= $this->business->search($id);
        return view('headquarters/index', compact('business'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $business= $this->business->search($id);
        return view('headquarters/create', compact('business'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $business_id)
    {
        $this->validate($request, $this->headquarterStoreRules());
        $business= $this->business->search($business_id);
        $headquarter= $this->headquarter->create([
            'business_id'=> $business->id,
            'name'=> $request->headquarter_name,
            'description'=> $request->headquarter_description,
            'direction'=> $request->headquarter_direction,
        ]);

        return redirect()->to('system/headquarters/'.$business->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function spaceManagement($id)
    {
        //Do refactoring
        $headquarter= $this->headquarter->search($id);
        return view('headquarters/space_management', compact('headquarter'));
    }

    public function searchByBusiness($business_id)
    {
        $headquarters= $this->headquarter->getModel()->where('business_id', '=', $business_id)->get();
        return $headquarters;
    }
}
