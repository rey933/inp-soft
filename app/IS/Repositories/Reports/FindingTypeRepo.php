<?php
/**
 * Created by Carlos José Rey Sayago.
 * User: crey
 * Date: 20/10/2016
 * Time: 02:17 PM
 */

namespace App\IS\Repositories\Reports;


use App\IS\Repositories\Base\BaseRepo;

class FindingTypeRepo extends BaseRepo
{

    public function getModel()
    {
        return new FindingType();
    }

    public function getTypes()
    {
        $types_collection= $this->all();
        foreach ($types_collection as $type) {
            if($this->haveFindings($type->id)) $types[]= $type->title;
        }
        return $types;
    }


    //Logic
    /**
     * @param $type_id
     * @return bool
     */
    public function haveFindings($type_id)
    {
        $type= $this->search($type_id);
        $findings_qty= $type->findings()->count();
        if($findings_qty > 0) return true;

        return false;
    }

    public function findingsQtyByType()
    {
        $types= $this->all();

        foreach ($types as $type) {
            if($this->haveFindings($type->id)) {
                $findings_qty[]= $type->findings()->get()->count();
            }
        }

        return $findings_qty;
    }
}