@extends('layouts.master')

    @section('content_title')
        Editar datos de la empresa
    @stop

    @section('bread')
        <li><a href="{!! url('system/dashboard') !!}">Dashboard</a></li>
        <li><a href="{!! url('system/businesses/') !!}">Empresas</a></li>
        <li class="active">Editar datos de: {!! $business->name !!}</li>
    @stop

    @section('content')
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {!! Form::open(['method'=> 'PUT','action'=> ['Businesses\BusinessController@update', 'id'=> $business->id], 'files'=> true,
                        'id'=> 'update_form', 'onsubmit'=> 'return false']) !!}
            <div>
                <div class="form-group">
                    {!! Form::label('business_name', 'Nombre de la Empresa') !!}
                    {!! Form::text('business_name', $business->name, ['class'=> 'form-control required',
                                                           'placeholder'=> 'Ejemplo: Plumrose Latinoamericana C.A.']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('business_rif', 'Rif de la Empresa') !!}
                    {!! Form::text('business_rif', $business->rif, ['class'=> 'form-control required',
                                                           'placeholder'=> 'Ejemplo: J-25625165-1']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('business_logo', 'Logo de la Empresa') !!}
                    {!! Form::file('business_logo') !!}
                    <p class="help-block">En caso de no seleccionar un nuevo logo, será conservado el anterior. (Máx: 600kb)</p>
                </div>
                <div class="form-group">
                    {!! Form::label('business_description', 'Descripción de la Empresa') !!}
                    {!! Form::textarea('business_description', $business->description, ['class'=> 'form-control required',
                                                           'placeholder'=> 'Ejemplo: Empresa encargada de la producción, fabricación y empaque de embutidos',
                                                           'rows'=> 3]) !!}
                </div>
                <div class="form-group">
                    <a href="{!! action('Businesses\BusinessController@index') !!}" class="btn btn-default btn-sm">Volver</a>
                    {!! Form::button('Actualizar datos de la Empresa', ['id'=> 'update_btn', 'class'=> 'btn btn-success btn-sm']) !!}
                </div>
            </div>
        {!! Form::close() !!}
    @stop

    @section('post_script')
        {!! Html::script('js/jquery.steps.js') !!}
        {!! Html::script('js/localization/messages_es.js') !!}

        <script>
            $(document).ready(function() {
                var form= $('#update_form');
                $('#update_btn').click(function() {
                    form.removeAttr('onsubmit');
                    form.validate();
                    if(form.valid()) {
                        form.submit();
                    } else{
                        return form.valid();
                    }
                });
            });
        </script>
    @stop