<?php
/**
 * Created by Carlos José Rey Sayago.
 * User: crey
 * Date: 15/09/2016
 * Time: 07:39 AM
 */

namespace App\IS\Repositories\Reports;


use App\IS\Repositories\Base\BaseRepo;

class FindingRepo extends BaseRepo
{
    /**
     * Get the Model
     * @return Finding
     */
    public function getModel()
    {
        return new Finding();
    }

    /**
     * Get the Full name of the reporter
     * @param $report_id
     * @return string
     */
    public function getReporterFullName($report_id)
    {
    	$report= $this->search($report_id);
    	$reporter= $report->reporter()->first();
    	$full_name= $reporter->last_name . ', ' . $reporter->name;
    	return $full_name;
    }

    public function getFindingBusiness($report_id)
    {
    	$report= $this->search($report_id);
    	$business= $report->block()->first()->headquarter()->first()->business()->first()->name;
    	return $business;
    }

    public function getFindingHeadquarter($report_id)
    {
    	$report= $this->search($report_id);
    	$headquarter= $report->block()->first()->headquarter()->first()->name;
    	return $headquarter;
    }

    public function getFindingBlock($report_id)
    {
    	$report= $this->search($report_id);
    	$headquarter= $report->block()->first()->name;
    	return $headquarter;
    }

    public function getFindingDepartment($report_id)
    {
    	$report= $this->search($report_id);
    	$department= $report->department()->first()->name;
    	return $department;
    }

    public function getFindingPlace($report_id)
    {
    	$report= $this->search($report_id);
    	$place= $report->place()->first()->name;
    	return $place;
    }

    public function getFindingPlan($report_id)
    {
    	$report= $this->search($report_id);
    	$plan= $report->plan()->first();
    	return $plan;
    }

    /**
     * Return the status of a Finding
     *  If the finding have all tasks in: "Completed" that will be the finding status
     *   If the finding have all tasks in: "Pendent" that will be the finding status
     *    If the finding have at least one task in "In Progress" that will be the finding status
     * @param $finding_id
     * @return String $finding_status
     */
    public function getFindingStatus($finding_id)
    {
        $finding= $this->search($finding_id);
        $tasks= $finding->plan()->first()->tasks()->get();

        $finding_status= '';

        $pendent_counter= 0;
        $process_counter= 0;
        $completed_counter= 0;

        //Some Findings does not have tasks
        if($tasks->count() == 0 ) return $finding_status= 'Pendent';
        //---------------------------------

        foreach ($tasks as $task) {
            if ($task->status == 'Pendent') $pendent_counter ++;
            if ($task->status == 'In Progress') $process_counter ++;
            if ($task->status == 'Finished') $completed_counter ++;
        }

        if($pendent_counter > 0 && $process_counter == 0 && $completed_counter == 0) {
            $finding_status= 'Pendent';
        }
        elseif ($process_counter > 0 || ($pendent_counter > 0 && $completed_counter > 0 )) {
            $finding_status= 'In Progress';
        }
        elseif($completed_counter > 0 && $process_counter == 0 && $pendent_counter == 0) {
            $finding_status= 'Finished';
        }

        return $finding_status;
    }

    /**
     * Get the finding Risk
     * @param $finding_id
     * @return mixed
     */
    public function getFindingRisk($finding_id)
    {
        $finding= $this->search($finding_id);
        return $finding->risk_level;
    }

    //Logic

    public function reportedFindings($headquarter_id)
    {
        return $findings= $this->getModel()->where('headquarter_id', '=', $headquarter_id)->count();
    }

    /**
     * Check for the completion of all finding tasks
     * @param $finding_id
     * @return bool
     */
    public function isCorrected($finding_id)
    {
        $finding= $this->search($finding_id);
        $tasks= $finding->plan()->first()->tasks()->get();

        foreach ($tasks as $task) {
            if($task->status == 'Finished') return true;
        }
        return false; //Is corrected
    }

    /**
     * Get the Corrected Findings quantity
     * @param $headquarter_id
     * @return int
     */
    public function correctedFindings($headquarter_id)
    {
        $findings= $this->getModel()->where('headquarter_id', '=', $headquarter_id)->get();
        $corrected_findings_qty= 0;

        foreach ($findings as $finding) {
            if($this->isCorrected($finding->id)) {
                $corrected_findings_qty ++;
            }
        }
        return $corrected_findings_qty;
    }

    /**
     * Get the number of Pendent Findings
     * @param $headquarter_id
     * @return int
     */
    public function pendentFindings($headquarter_id) {
        $findings= $this->getModel()->where('headquarter_id', '=', $headquarter_id)->get();
        $pendent_findings_qty= 0;

        foreach ($findings as $finding) {
            if($this->getFindingStatus($finding->id) == 'Pendent') {
                $pendent_findings_qty ++;
            }
        }

        return $pendent_findings_qty;
    }

    /**
     * Get the number of In Progress Findings
     * @param $headquarter_id
     * @return int
     */
    public function inProgressFindings($headquarter_id) {
        $findings= $this->getModel()->where('headquarter_id', '=', $headquarter_id)->get();
        $progress_findings_qty= 0;

        foreach ($findings as $finding) {
            if($this->getFindingStatus($finding->id) == 'In Progress') {
                $progress_findings_qty ++;
            }
        }

        return $progress_findings_qty;
    }

    /**
     * Get the number of Completed Findings
     * @param $headquarter_id
     * @return int
     */
    public function completedFindings($headquarter_id)
    {
        $findings= $this->getModel()->where('headquarter_id', '=', $headquarter_id)->get();
        $completed_findings_qty= 0;

        foreach ($findings as $finding) {
            if($this->getFindingStatus($finding->id) == 'Finished') {
                $completed_findings_qty ++;
            }
        }

        return $completed_findings_qty;
    }

    /**
     * Get the number of Low Risk Findings
     * @param $headquarter_id
     * @return int
     */
    public function lowRiskFindings($headquarter_id)
    {
        $findings= $this->getModel()->where('headquarter_id', '=', $headquarter_id)->get();
        $low_risk_findings_qty= 0;

        foreach ($findings as $finding) {
            if($finding->risk_level == 'Leve') $low_risk_findings_qty ++;
        }

        return $low_risk_findings_qty;
    }

    /**
     * Get the number of Moderate Risk Findings
     * @param $headquarter_id
     * @return int
     */
    public function moderateRiskFindings($headquarter_id)
    {
        $findings= $this->getModel()->where('headquarter_id', '=', $headquarter_id)->get();
        $moderate_risk_findings_qty= 0;

        foreach ($findings as $finding) {
            if($finding->risk_level == 'Moderado') $moderate_risk_findings_qty ++;
        }

        return $moderate_risk_findings_qty;
    }

    /**
     * Get the number of High Risk Findings
     * @param $headquarter_id
     * @return int
     */
    public function highRiskFindings($headquarter_id)
    {
        $findings= $this->getModel()->where('headquarter_id', '=', $headquarter_id)->get();
        $high_risk_findings_qty= 0;

        foreach ($findings as $finding) {
            if($finding->risk_level == 'Alto') $high_risk_findings_qty ++;
        }

        return $high_risk_findings_qty;
    }

    /**
     * Get the number of Intolerable Risk Findings
     * @param $headquarter_id
     * @return int
     */
    public function intolerableRiskFindings($headquarter_id)
    {
        $findings= $this->getModel()->where('headquarter_id', '=', $headquarter_id)->get();
        $intolerable_risk_findings_qty= 0;

        foreach ($findings as $finding) {
            if($finding->risk_level == 'Intolerable') $intolerable_risk_findings_qty ++;
        }

        return $intolerable_risk_findings_qty;
    }

    public function correctedFindingsPercent($headquarter_id)
    {
        $findings= $this->getModel()->where('headquarter_id', '=', $headquarter_id)->get();
        $findings_qty= $findings->count();
        $corrected_findings_qty= $this->correctedFindings($headquarter_id);

        $percent= ($corrected_findings_qty * 100) / $findings_qty;
        return $percent;
    }

    /**
     * @param $headquarter_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function findingsQtyByReporter($headquarter_id, $graphic_type)
    {
        $findings= $this->getModel()->where('headquarter_id', '=', $headquarter_id)->get();
        $previous_reporter= 0;
        foreach ($findings as $finding) {
            $actual_reporter= $finding->reporter()->first()->identity;
            if($finding->reporter()->first()->findings()->count() > 0) {
                if($actual_reporter != $previous_reporter) {
                    $reporters[]= $this->getReporterFullName($finding->id);
                    $findings_qty[]= $finding->reporter()->first()->findings()->count();
                    $previous_reporter= $actual_reporter;
                }
            }
        }
        return response()->json([
            'reporters'=> $reporters,
            'findings_qty'=> $findings_qty,
            'graph_type'=> $graphic_type
        ]);
    }

    public function findingsQtyByArea($headquarter_id, $graph_type)
    {
        $findings= $this->getModel()->where('headquarter_id', '=', $headquarter_id)->get();
        $registered_data= [];
        foreach ($findings as $finding) {
            $actual_area_id= $finding->area()->first()->id;
            if(!in_array($actual_area_id, $registered_data)) {
                $areas[]= $finding->area()->first()->name;
                $findings_qty[]= $finding->area()->first()->findings()->count();
                $registered_data[]= $actual_area_id;
            }
        }

        return response()->json([
            'areas'=> $areas,
            'findings_qty'=>  $findings_qty,
            'graph_type'=> $graph_type,
        ]);
    }
}