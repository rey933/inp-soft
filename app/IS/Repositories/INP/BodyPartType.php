<?php

namespace App\IS\Repositories\INP;

use Illuminate\Database\Eloquent\Model;

class BodyPartType extends Model
{
    protected $table= 'inpsasel-body-parts';
}
