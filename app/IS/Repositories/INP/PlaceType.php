<?php

namespace App\IS\Repositories\INP;

use Illuminate\Database\Eloquent\Model;

class PlaceType extends Model
{
    protected $table= 'inpsasel-places-type';
}
