<?php

namespace App\IS\Repositories\INP;

use Illuminate\Database\Eloquent\Model;

class JobType extends Model
{
    protected $table= 'inpsasel-jobs-type';
}
