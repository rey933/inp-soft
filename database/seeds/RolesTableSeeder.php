<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'admin',
            'description' => 'This user can make CRUD to the entire system, also can watch system statistics',
        ]);

        DB::table('roles')->insert([
            'name' => 'manager',
            'description' => 'This user can make CRUD to the Reports and Coordinators, also can watch Reports statistics and approve intolerable Reports',
        ]);

        DB::table('roles')->insert([
            'name' => 'coordinator',
            'description' => 'This user can make CRUD to Reports',
        ]);
    }
}
