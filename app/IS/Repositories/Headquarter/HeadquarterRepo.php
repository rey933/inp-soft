<?php
/**
 * Created by Carlos José Rey Sayago.
 * User: crey
 * Date: 19/09/2016
 * Time: 08:58 AM
 */

namespace App\IS\Repositories\Headquarter;


use App\IS\Repositories\Base\BaseRepo;

class HeadquarterRepo extends BaseRepo
{
    protected $headquarter;
//    public function __construct($headquarter_id)
//    {
//        $this->headquarter= $this->search($headquarter_id);
//    }

    public function getModel()
    {
        return new Headquarter();
    }

    public function getBlocksNum()
    {
    	$num_blocks= $this->getModel()->blocks()->count();
    	return $num_blocks;
    }

    //---Presenters

    public function name()
    {
    	$name= $this->getModel()->name;
    	return $name;
    }
}