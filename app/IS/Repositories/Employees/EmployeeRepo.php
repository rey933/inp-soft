<?php
/**
 * Created by Carlos José Rey Sayago
 * User: cjrey
 * Date: 2016/09/10
 * Time: 15:31
 */

namespace App\IS\Repositories\Employees;


use App\IS\Repositories\Base\BaseRepo;

class EmployeeRepo extends BaseRepo
{
    /**
     * @return Employee
     */
    public function getModel()
    {
        return new Employee();
    }


    /**
     * @param Employee $employee
     * @return string
     */
    public function getSupervisor(Employee $employee)
    {
        $supervisor= $employee->where('identity', '=', $employee->supervisor_identity);
        if($supervisor->count() > 0) {
            return $supervisor->get();
        } else {
            return 'Error 0x0001: No se han encontrado coincidencias - <a href="mailto:sistemas@plumrose.com">Reporta este error</a>';
        }
    }

    public function getEmployeeFullName($identity)
    {
        $employee= $this->getModel()->where('identity', '=', $identity)->first();

        $full_name= $employee->last_name . ', ' . $employee->name;
        return $full_name;
    }
}