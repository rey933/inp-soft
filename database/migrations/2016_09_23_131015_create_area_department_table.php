<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaDepartmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_department', function(Blueprint $table) {
           $table->increments('id');
           $table->integer('area_id')->unsigned();
           $table->foreign('area_id')
                 ->references('id')
                 ->on('areas');
           $table->integer('department_id')->unsigned();
           $table->foreign('department_id')
                 ->references('id')
                 ->on('departments');
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('area_department');
    }
}
