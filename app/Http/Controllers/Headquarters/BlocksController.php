<?php

namespace App\Http\Controllers\Headquarters;

use App\IS\Repositories\Blocks\BlockRepo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BlocksController extends Controller
{
    protected $block;

    public function __construct(BlockRepo $block)
    {
        $this->block= $block;
    }

    public function all()
    {
        return $this->block->all();
    }

    public function store($headquarter_id, Request $request)
    {
        $data= $this->block->create([
            'headquarter_id'=> $headquarter_id,
            'name'=> $request->block_name,
            'description'=> $request->block_description,
        ]);
        return response()->json([
            'block'=> [
                'id'=> $data->id,
                'name'=> $data->name,
                'description'=> $data->description,
            ],
        ]);
    }

    public function delete($block_id)
    {
        $block= $this->block->search($block_id);
        $block->delete();
        return response()->json([
            'status'=> 'completed'
        ]);
    }

    /**
      * Search Blocks for the given $headquarter_id
      */
    public function search($headquarter_id)
    {
        $blocks= $this->block->getModel()->where('headquarter_id', '=', $headquarter_id)->get();
        return $blocks;
    }
}
