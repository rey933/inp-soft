<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Findings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('findings', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('business_id')->unsigned();
            $table->foreign('business_id')
                ->references('id')
                ->on('businesses')
                ->onDelete('cascade');
            $table->integer('headquarter_id')->unsigned();
            $table->foreign('headquarter_id')
                ->references('id')
                ->on('headquarters')
                ->onDelete('cascade');
            $table->integer('block_id')->unsigned();
            $table->foreign('block_id')
                  ->references('id')
                  ->on('blocks')
                  ->onDelete('cascade');
            $table->integer('department_id')->unsigned();
            $table->foreign('department_id')
                  ->references('id')
                  ->on('departments')
                  ->onDelete('cascade');
            $table->integer('area_id')->unsigned();
            $table->foreign('area_id')
                  ->references('id')
                  ->on('areas')
                  ->onDelete('cascade');
            $table->integer('place_id')->unsigned();
            $table->foreign('place_id')
                  ->references('id')
                  ->on('places')
                  ->onDelete('cascade');
            $table->string('reporter'); //Reporter identity from employees table
            $table->foreign('reporter')
                  ->references('identity')
                  ->on('employees')
                  ->onDelete('cascade');
            $table->text('statement');
            $table->integer('type_id')->unsigned();
            $table->foreign('type_id')
                  ->references('id')
                  ->on('finding_types')
                  ->onDelete('cascade');
            $table->enum('risk_level', [
                'Leve', 'Moderado', 'Alto', 'Intolerable',
            ]);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('findings');
    }
}
