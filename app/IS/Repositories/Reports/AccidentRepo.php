<?php
/**
 * Created by Carlos José Rey Sayago.
 * User: crey
 * Date: 15/09/2016
 * Time: 07:39 AM
 */

namespace App\IS\Repositories\Reports;


use App\IS\Repositories\Base\BaseRepo;

class AccidentRepo extends BaseRepo
{
    /**
     * Get the Model
     * @return Finding
     */
    public function getModel()
    {
        return new Accident();
    }
}