@extends('layouts.master')
    @section('post_style')
        <style>
            .image {
                background: url("{!! asset('img/content-images/accidents/reportes.jpg') !!}") no-repeat;
                background-size: 100%;
                height: 70px;
                margin: 0 auto;
            }
        </style>
    @stop
    @section('content_title')
        Mis Reportes de Accidentes
    @stop

    @section('bread')
        <li><a href="{!! url('system/dashboard') !!}">Dashboard</a></li>
        <li class="active">Mis Reportes de Accidentes</li>
    @stop

    @section('content')
        <div class="image"></div>
        <table class="table table-striped">
            <thead>
                <th>Fecha del Accidente</th>
                <th>Hora del Accidente</th>
                <th>Afectado</th>
                <th>Clasificación</th>
                <th>Categoría</th>
                <th>Acciones</th>
            </thead>
            <tbody>
                @foreach($accidents as $accident)
                    <tr>
                        <td>{!! $accident->accident_date !!}</td>
                        <td>{!! $accident->accident_hour !!}</td>
                        <td>{!! $employee_repo->getEmployeeFullName($accident->affected_identity) !!}</td>
                        <td>{!! $accident->accident_classification !!}</td>
                        <td>{!! $accident->accident_category !!}</td>
                        <td>
                            <a href="#" class="btn btn-default btn-sm change-status-btn"
                                    title="Ver Datos Completos">
                            <i class="fa fa-eye"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {!! $accidents->links() !!}
    @stop