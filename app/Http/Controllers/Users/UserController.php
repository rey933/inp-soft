<?php

namespace App\Http\Controllers\Users;

use App\IS\Repositories\Users\UserRepo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    protected $user;

    public function __construct(UserRepo $user)
    {
        $this->user= $user;
    }

    public function getUser($id)
    {
        return $this->user->search($id);
    }

    public function removeUser($id)
    {
        return $this->user->remove($id);
    }
}
