<?php

namespace App\Http\Controllers\Reports;

use App\IS\Repositories\Businesses\BusinessRepo;
use App\IS\Repositories\Employees\EmployeeRepo;
use App\IS\Repositories\INP\AccidentType;
use App\IS\Repositories\INP\BodyPartType;
use App\IS\Repositories\INP\InjureType;
use App\IS\Repositories\INP\JobType;
use App\IS\Repositories\INP\MaterialAgentType;
use App\IS\Repositories\INP\PhysicalActivityType;
use App\IS\Repositories\INP\PlaceType;
use App\IS\Repositories\Places\PlaceRepo;
use App\IS\Repositories\Reports\AccidentRepo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AccidentsController extends Controller
{
    protected $business;
    protected $accident;
    protected $place;
    protected $employee;
    protected $accident_type;
    protected $body_part_type;
    protected $injure_type;
    protected $job_type;
    protected $material_agent_type;
    protected $physical_activity_type;
    protected $place_type;

    public function __construct(BusinessRepo $business, AccidentRepo $accident, PlaceRepo $place, EmployeeRepo $employee)
    {
        $this->business= $business;
        $this->accident= $accident;
        $this->place= $place;
        $this->employee= $employee;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user= Auth::user();
        $employee_repo= $this->employee;
        $accidents= $this->accident->getModel()->where('reporter', '=', $user->employee_identity)->paginate(5);
        return view('reports.accidents.index', ['accidents'=> $accidents, 'employee_repo'=> $employee_repo]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $businesses= $this->business->all();
        $accident_types= AccidentType::orderBy('title', 'asc')->pluck('title', 'id');
        $body_part_types= BodyPartType::orderBy('title', 'asc')->pluck('title', 'id');
        $injure_types= InjureType::orderBy('title', 'asc')->pluck('title', 'id');
        $job_types= JobType::orderBy('title', 'asc')->pluck('title', 'id');
        $material_agent_types= MaterialAgentType::orderBy('title', 'asc')->pluck('title', 'id');
        $physical_activity_types= PhysicalActivityType::orderBy('title', 'asc')->pluck('title', 'id');
        $place_types= PlaceType::orderBy('title', 'asc')->pluck('title', 'id');
        return view('reports/accidents/create', compact('businesses', 'accident_types', 'body_part_types', 'injure_types', 'job_types', 'material_agent_types', 'physical_activity_types', 'place_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user= Auth::user();

        $accident_hour= str_replace(" ", "", $request->accident_hour);

        //Creating the Place
        $place= $this->place->create([
            'area_id'=> $request->area_id,
            'name'=> $request->place,
        ]);

        $data= [
            'reporter'=> $user->employee_identity,
            'affected_identity'=> $request->employee,
            'accident_date'=> $request->accident_date,
            'accident_hour'=> $accident_hour,
            'accident_classification'=> $request->accident_classification,
            'accident_category'=> $request->accident_category,
            'business_id'=> $request->business_id,
            'headquarter_id'=> $request->headquarter_id,
            'block_id'=> $request->block_id,
            'department_id'=> $request->department_id,
            'area_id'=> $request->area_id,
            'place_id'=> $place->id,
            'place_type_id'=> $request->place_type,
            'specific_place_type'=> $request->specific_place_type,
            'physical_activity_type_id'=> $request->physical_activity_type,
            'specific_activity_type'=> $request->specific_activity_type,
            'job_type_id'=> $request->job_type,
            'specific_job_type'=> $request->specific_job_type,
            'material_agent_id'=> $request->material_agent,
            'specific_material_agent'=> $request->specific_material_agent,
            'accident_id'=> $request->accident_type,
            'specific_accident'=> $request->specific_accident_type,
            'body_part_id'=> $request->body_part,
            'specific_body_part'=> $request->specific_body_part,
            'injure_id'=> $request->injure,
            'specific_injure'=> $request->specific_injure,
            'severity'=> $request->severity,
            'repose_days'=> $request->repose_days,
        ];
        $this->accident->create($data);

        return redirect()->to('system/reports/accidents/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
