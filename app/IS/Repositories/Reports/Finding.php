<?php

namespace App\IS\Repositories\Reports;

use App\IS\Repositories\Areas\Area;
use App\IS\Repositories\Base\BaseModel;

class Finding extends BaseModel
{
    protected $table= 'findings';
    protected $fillable= [
    	'business_id', 'headquarter_id', 'block_id', 'department_id', 'area_id', 'place_id', 'reporter', 'statement', 'type_id', 'report_date', 'risk_level',
    ];

    public function reporter()
    {
    	return $this->belongsTo('App\IS\Repositories\Employees\Employee', 'reporter', 'identity');
    }

    public function block()
    {
    	return $this->belongsTo('App\IS\Repositories\Blocks\Block', 'block_id');
    }

    public function department()
    {
    	return $this->belongsTo('App\IS\Repositories\Departments\Department', 'department_id');
    }

    public function area()
    {
        return $this->belongsTo(Area::class, 'area_id');
    }

    public function place()
    {
    	return $this->belongsTo('App\IS\Repositories\Places\Place', 'place_id');
    }

    public function plan()
    {
        return $this->hasOne('App\IS\Repositories\Plans\Plan', 'finding_id');
    }
    
}
