<?php

namespace App\IS\Repositories\Tasks;

use App\IS\Repositories\Base\BaseModel;

class Task extends BaseModel
{
    protected $table= 'tasks';

    protected $fillable= [
    	'plan_id', 'mandated', 'title', 'body', 'proposed_completion_date', 'completion_date', 'status',
    ];

    public function plan()
    {
    	return $this->belongsTo('App\IS\Repositories\Plans\Plan', 'plan_id');
    }
}
