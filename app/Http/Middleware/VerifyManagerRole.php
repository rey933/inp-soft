<?php

namespace App\Http\Middleware;

use Closure;

class VerifyManagerRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->role()->first()->name != 'admin') {
            if($request->user()->role()->first()->name != 'manager') {
                return redirect('system/dashboard')->with(['errors' => ['No tiene los permisos suficientes para acceder a esta zona']]);
            }
        }
        return $next($request);
    }
}
