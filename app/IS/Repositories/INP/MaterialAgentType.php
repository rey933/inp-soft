<?php

namespace App\IS\Repositories\INP;

use Illuminate\Database\Eloquent\Model;

class MaterialAgentType extends Model
{
    protected $table= 'inpsasel-material-agents-type';
}
