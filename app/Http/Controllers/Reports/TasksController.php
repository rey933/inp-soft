<?php

namespace App\Http\Controllers\Reports;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\IS\Repositories\Tasks\TaskRepo; 

class TasksController extends Controller
{
    protected $task;

    /**
     * TasksController constructor.
     * @param TaskRepo $task
     */
    public function __construct(TaskRepo $task)
    {
        $this->task= $task;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks= $this->task->all();
        return $tasks;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task= $this->task->create([
            'plan_id'=> $request->plan_id, 
            'mandated'=> $request->mandatory_identity, 
            'title'=> $request->task_title, 
            'body'=> $request->task_body, 
            'proposed_completion_date'=> $request->task_solution_date, 
            'completion_date'=> $request->task_solution_date, 
            'status'=> 'Pendent',
        ]);
        return $task;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Change the current task status
     * @param $id
     * @return mixed
     */
    public function updateStatus($id, $new_status)
    {
        $task= $this->task->search($id);
        $task->status= $new_status;

        $task->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task= $this->task->search($id);
        $task->delete();
    }
}
