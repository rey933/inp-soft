<?php

namespace App\Http\Controllers\Auth;

use App\IS\Repositories\Roles\RoleRepo;
use App\IS\Repositories\Users\UserRepo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    protected $user;
    protected $role;

    public function __construct(UserRepo $user, RoleRepo $role)
    {
        $this->user= $user;
        $this->role= $role;
    }

    /**
     * This function will display a registration form that allow
     *  register all kind of usrs
     * @param Request $request
     * @return mixed
     */
    public function registerAllTransaction(Request $request)
    {
        return view('auth/register_all_form');
    }

    /**
     * This function will display a registration form that allow
     *  register Managers and Coordinators users
     * @param Request $request
     * @return mixed
     */
    public function registerManagerTransaction(Request $request)
    {
        return $request->user()->role()->get();
    }


    /**
     * This function make registration process
     * @param Request $request
     * @return mixed
     */
    public function register(Request $request)
    {
        $data= $request->all(); //Getting all inserted data
        $data['password']= bcrypt($data['password']); //Hashing the password
        $user= $this->user->create($data); //Creating the user
        return $user;
    }
}
