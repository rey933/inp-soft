<!DOCTYPE html>
    <html lang="es">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>@yield('title', 'Sistema de Hallazgos y Accidentes')</title>

        <!-- Bootstrap Core CSS -->
        {!! Html::style('css/bootstrap.min.css') !!}

        <!-- MetisMenu CSS -->
        {!! Html::style('css/metisMenu.min.css') !!}

        <!-- Custom CSS -->
        {!! Html::style('css/sb-admin-2.min.css') !!}

        <!-- Custom Fonts -->
        {!! Html::style('font-awesome/css/font-awesome.css') !!}

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .error{
                color: #8a1f11;
            }
            #loading_wrap{
                background: rgba(0, 0, 0, .5);
                z-index: 9999 !important;
                display: none;
            }
                #loading_wrap .loader{
                    width: 150px;
                    height: 250px;
                    margin: 300px auto;
                    text-align: center;
                }
        </style>
        @yield('post_style')

    </head>

    <body>
    <div id='loading_wrap' style='position:fixed; height:100%; width:100%; overflow:hidden; top:0; left:0;'>
        <div class="loader">
            <img src="{!! asset('img/content-images/loader.gif') !!}" alt="Loader">
        </div>
    </div>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html"> <i class="fa fa-code"></i> Sistema de Hallazgos y Accidentes</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="{!! url('logout') !!}"><i class="fa fa-sign-out fa-fw"></i>Cerrar Sesión</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="{!! url('system/dashboard') !!}"><i class="fa fa-dashboard fa-fw"></i> Balance</a>
                        </li>
                        <li>
                            <a href="#!"><i class="fa fa-edit"></i> Reportes <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{!! url('system/reports/findings/create') !!}">Buscador de Reportes</a>
                                    <a href="#!">Listado global de Reportes</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#!"><i class="fa fa-ambulance"></i> Accidentes <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{!! url('system/reports/accidents/') !!}">Mis reportes de Accidentes</a>
                                </li>
                                <li>
                                    <a href="{!! url('system/reports/accidents/create') !!}">Reportar Accidente</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#!"><i class="fa fa-warning"></i> Hallazgos <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{!! url('system/reports/findings/') !!}">Mis Reportes de Hallazgos</a>
                                </li>
                                <li>
                                    <a href="{!! url('system/reports/findings/create') !!}">Reportar de Hallazgo</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#!"><i class="fa fa-building-o"></i> Organizaciónes <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{!! url('system/businesses') !!}">Empresas</a>
                                </li>
                                <li>
                                    <a href="{!! url('system/businesses/statistics') !!}">Estadísticas</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="index.html"><i class="fa fa-terminal"></i> Sistema <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#!">Log de Modificaciones</a>
                                </li>
                                <li>
                                    <a href="#!">Log de Sincronizaciones</a>
                                </li>
                                <li>
                                    <a href="#!">Variables del Sistema</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                        <li>
                            <a href="index.html"><i class="fa fa-users"></i> Usuarios</a>
                        </li>
                        <!--<li>
                            <a href="index.html"><i class="fa fa-comment"></i> Chat</a>
                        </li>-->
                        <li>
                            <a href="index.html"><i class="fa fa-stethoscope"></i> Soporte <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">Manual de Uso</a>
                                </li>
                                <li>
                                    <a href="morris.html">Manual de Desarrollo</a>
                                </li>
                                <li>
                                    <a href="morris.html">Contactar con Soporte</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">@yield('content_title', 'Título')</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <ol class="breadcrumb">
                            @yield('bread')
                            <li></li>
                        </ol>
                        @yield('content')
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    {!! Html::script('js/jquery-3.1.0.js') !!}
    <!--{!! Html::script('js/jquery-11.0.min.js') !!}-->

    <!-- Bootstrap Core JavaScript -->
    {!! Html::script('js/bootstrap.min.js') !!}

    <!-- Metis Menu Plugin JavaScript -->
    {!! Html::script('js/metisMenu.min.js') !!}

    <!-- Custom Theme JavaScript -->
    {!! Html::script('js/sb-admin-2.min.js') !!}

    <!-- Jquery Validate Plugin -->
    {!! Html::script('js/jquery.validate.js') !!}

    <!-- Vuejs -->
    {!! Html::script('js/vue.js') !!}

    @yield('post_script')

    <!-- Main.js -->
    {!! Html::script('js/main.js') !!}
    
    </body>

</html>
