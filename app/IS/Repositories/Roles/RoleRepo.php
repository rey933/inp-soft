<?php
/**
 * Created by Carlos José Rey Sayago.
 * User: crey
 * Date: 15/09/2016
 * Time: 07:39 AM
 */

namespace App\IS\Repositories\Roles;


use App\IS\Repositories\Base\BaseRepo;

class RoleRepo extends BaseRepo
{

    public function getModel()
    {
        return new Role();
    }
}