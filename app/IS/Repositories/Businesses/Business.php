<?php

namespace App\IS\Repositories\Businesses;

use App\IS\Repositories\Base\BaseModel;
use Illuminate\Database\Eloquent\Model;

class Business extends BaseModel
{
    protected $table= 'businesses';
    protected $fillable= [
        'name', 'rif', 'description', 'image'
    ];

    public function headquarters()
    {
        return $this->hasMany('App\IS\Repositories\Headquarter\Headquarter', 'business_id');
    }

    public function findings()
    {
        return $this->hasMany('App\IS\Repositories\Reports\Finding', 'business_id');
    }
}
