@extends('layouts.master')
    @if($businesses->count() > 0)
        @section('content_title')
            Listado de Empresas
        @stop

        @section('bread')
            <li><a href="{!! url('system/dashboard') !!}">Dashboard</a></li>
            <li class="active">Empresas</li>
        @stop

        @section('content')

            <table class="table table-striped">
                <thead>
                    <th></th>
                    <th>Nombre de la Empresa</th>
                    <th>Número de Sucursales</th>
                    <th>Acciones</th>
                </thead>

                <tbody>
                    @foreach($businesses as $business)
                        <tr>
                            <td><img src="{!! asset('img/content-images/businesses/logos/'.$business->image) !!}"
                                     alt=""
                                     style="width: 40px;"></td>
                            <td>{!! $business->name !!}</td>
                            <td>
                                <a href="{!! action('Headquarters\HeadquarterController@index', ['id'=> $business->id]) !!}"
                                   title="Administrar sucursales">
                                    {!! $business->headquarters()->count() !!}
                                </a>
                            </td>
                            <td>
                                <a href="{!! action('Businesses\BusinessController@edit', ['id'=> $business->id]) !!}"
                                   class="btn btn-default btn-sm" title="Editar Empresa">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="#" class="btn btn-danger btn-sm" title="Eliminar Empresa"><i class="fa fa-minus"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <a href="{!! action('Businesses\BusinessController@create') !!}"
               class="btn btn-primary btn-sm">
                Registrar una nueva Empresa
            </a>
        @stop
    @else
        @section('content_title')
            No existen empresas
        @stop

        @section('bread')
            <li class="active">Dashboard</li>
        @stop

        @section('content')
            <div class="alert alert-danger">
                <strong>No ha sido registrada aún ninguna empresa</strong>
                <p>
                    Si desea acceder a la creación de reportes, visualización de estadísticas y comprobación de resultados.
                    Debe por lo menos crear una Empresa.
                </p>
                <br>
                <a href="{!! url('system/businesses/create') !!}" class="btn btn-success btn-sm" title="Crear Empresa"><i class="fa fa-pencil"></i></a>
                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-user-md" title="Contactar a Soporte Técnico"></i></a>
            </div>
        @stop
    @endif