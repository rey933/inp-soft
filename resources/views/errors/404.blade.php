<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Página no Encontrada</title>
    {!! Html::style('css/404.css') !!}
</head>
<body>

    <div class="wrapper row2">
        <div id="container" class="clear" style="width: 400px; margin:0 auto;">
            <!-- ####################################################################################################### -->
            <!-- ####################################################################################################### -->
            <!-- ####################################################################################################### -->
            <!-- ####################################################################################################### -->
            <div class="logo" style="text-align: center">
                <img src="{!! asset('img/content-images/logo-pr.png') !!}" alt="">
            </div>
            <section id="fof" class="clear">
                <!-- ####################################################################################################### -->
                <div class="fl_left">
                    <h1 style="text-align: center"><span>Error 404</span></h1>
                </div>
                <div class="fl_right">
                    <h2>Lo sentimos, página no encontrada:</h2>
                    <h4>Posibles Razones:</h4>
                    <ul>
                        <li>Ha ingresado una dirección inválida.</li>
                        <li>Ha seleccionado un enlace externo a la red Plumrose.</li>
                    </ul>
                </div>
                <p><a class="go-back" href="javascript:history.go(-1)">&laquo; Regresar</a> <strong>O</strong> <a class="go-home" href="{!! url('system/dashboard') !!}">Ir al Dashboard &raquo;</a></p>
                <!-- ####################################################################################################### -->
            </section>
            <!-- ####################################################################################################### -->
            <!-- ####################################################################################################### -->
            <!-- ####################################################################################################### -->
            <!-- ####################################################################################################### -->
        </div>
    </div>
</body>
</html>