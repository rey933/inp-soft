<?php

namespace App\Http\Controllers\Reports;

use App\IS\Repositories\Businesses\BusinessRepo;
use App\IS\Repositories\Reports\FindingTypeRepo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\IS\Repositories\Reports\FindingRepo;
use App\IS\Repositories\Places\PlaceRepo;
use App\IS\Repositories\Plans\PlanRepo;
use Illuminate\Support\Facades\Auth;

class FindingsController extends Controller
{
    protected $finding;
    protected $place;
    protected $plan;
    protected $type;
    protected $business;

    public function __construct(BusinessRepo $business, FindingRepo $finding, PlaceRepo $place, PlanRepo $plan, FindingTypeRepo $type)
    {
        $this->finding= $finding;
        $this->place= $place;
        $this->plan= $plan;
        $this->type= $type;
        $this->business= $business;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user= Auth::user();
        $findings= $this->finding->getModel()->where('reporter', '=', $user->employee_identity)->paginate(5);
        return view('reports.findings.index', ['findings'=> $findings]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $businesses= $this->business->all();
        $types= $this->type->getModel()->pluck('title', 'id');
        return view('reports.findings.create', compact('types', 'businesses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user= Auth::user();

        $place= $this->place->create([
            'area_id'=> $request->area_id,
            'name'=> $request->place,
        ]);
        //Creating the Finding
        $finding= $this->finding->create([
            'business_id'=> $request->business_id,
            'headquarter_id'=> $request->headquarter_id,
            'block_id'=> $request->block_id,
            'department_id'=> $request->department_id,
            'area_id'=> $request->area_id,
            'place_id'=> $place->id,
            'reporter'=> $user->employee_identity,
            'statement'=> $request->finding_statement,
            'type_id'=> $request->finding_type,
            'risk_level'=>$request->finding_risk_level,
        ]);

        //Creating the Plan
        if(isset($request->planification))
        {
            //If the finding need a resolution planning conference...
            $plan= $this->plan->create([
                'finding_id'=> $finding->id,
                'title'=> 'Plan de Acción: '.$finding->statement,
                'status'=> 'Planning',
            ]);
        } else {
            $plan= $this->plan->create([
                'finding_id'=> $finding->id,
                'title'=> 'Plan de Acción: '.$finding->statement,
                'status'=> 'Creating',
            ]);            
        }
        return redirect()->to('system/reports/findings/'.$finding->id);
    }

    /**
     * Get some statistics for all Findings
     * @param Request $request
     * @return array
     */
    public function statistics(Request $request)
    {
        switch ($request->graphic_type) {
            case 1:
                //Gráfico: Detectados / Corregidos
                $corrected_findings= $this->finding->correctedFindings($request->headquarter_id);
                $reported_findings= $this->finding->reportedFindings($request->headquarter_id);

                if($reported_findings > 0) {
                    return response()->json([
                        'corrected_findings'=> $corrected_findings,
                        'reported_findings'=> $reported_findings,
                        'graph_type'=> $request->graphic_type,
                    ]);
                } else {
                    return response()->json([
                        'error'=> 'No data',
                    ]);
                }

                break;
            case 2:
                $reported_findings= $this->finding->reportedFindings($request->headquarter_id);

                if($reported_findings > 0) {
                    return response()->json([
                        'corrected_porcent'=> round($this->finding->correctedFindingsPercent($request->headquarter_id), 2),
                        'graph_type'=> $request->graphic_type,
                    ]);
                } else {
                    return response()->json([
                        'error'=> 'No data',
                    ]);
                }
                break;
            case 3:
                //Gráfico: Distribución por estatus
                $pendent_findings= $this->finding->pendentFindings($request->headquarter_id);
                $progress_findings= $this->finding->inProgressFindings($request->headquarter_id);
                $completed_findings= $this->finding->completedFindings($request->headquarter_id);

                $reported_findings= $this->finding->reportedFindings($request->headquarter_id);

                if($reported_findings > 0) {
                    return response()->json([
                        'pendent_findings'=> $pendent_findings,
                        'progress_findings'=> $progress_findings,
                        'completed_findings'=> $completed_findings,
                        'graph_type'=> $request->graphic_type,
                    ]);
                } else {
                    return response()->json([
                        'error'=> 'No data',
                    ]);
                }
                break;
            case 4:
                $reported_findings= $this->finding->reportedFindings($request->headquarter_id);
                if($reported_findings > 0) {
                    $low_risk_findings = $this->finding->lowRiskFindings($request->headquarter_id);
                    $moderate_risk_findings = $this->finding->moderateRiskFindings($request->headquarter_id);
                    $high_risk_findings = $this->finding->highRiskFindings($request->headquarter_id);
                    $intolerable_risk_findings = $this->finding->intolerableRiskFindings($request->headquarter_id);
                    return response()->json([
                        'low_risk_findings' => $low_risk_findings,
                        'moderate_risk_findings' => $moderate_risk_findings,
                        'high_risk_findings' => $high_risk_findings,
                        'intolerable_risk_findings' => $intolerable_risk_findings,
                        'graph_type' => $request->graphic_type,
                    ]);
                } else{
                    return response()->json([
                        'error'=> 'No data',
                    ]);
                }
                break;
            case 5:
                $reported_findings= $this->finding->reportedFindings($request->headquarter_id);
                if($reported_findings > 0) {
                    return response()->json([
                        'types'=> $this->type->getTypes(),
                        'findings_qty'=> $this->type->findingsQtyByType(),
                        'graph_type' => $request->graphic_type,
                    ]);
                } else{
                    return response()->json([
                        'error'=> 'No data',
                    ]);
                }
                break;
            case 6:
                $reported_findings= $this->finding->reportedFindings($request->headquarter_id);
                if($reported_findings > 0) {
                    return $this->finding->findingsQtyByReporter($request->headquarter_id, $request->graphic_type);
                } else{
                    return response()->json([
                        'error'=> 'No data',
                    ]);
                }
            break;
            case 7:
                $reported_findings= $this->finding->reportedFindings($request->headquarter_id);
                if($reported_findings > 0) {
                    return $this->finding->findingsQtyByArea($request->headquarter_id, $request->graphic_type);
                } else{
                    return response()->json([
                        'error'=> 'No data',
                    ]);
                }
            break;
            case 8:
                return 'Gráfico 8';
                break;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $finding= $this->finding->search($id);
        $finding_repo= $this->finding;
        return view('reports.findings.show', compact('finding', 'finding_repo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
