@extends('layouts.master')
    @section('post_style')
        {!! Html::style('css/wizard.css') !!}
    @stop

    @section('content_title')
        Crear una nueva Empresa
    @stop

    @section('bread')
        <li><a href="{!! url('system/dashboard') !!}">Dashboard</a></li>
        <li><a href="{!! url('system/businesses/') !!}">Empresas</a></li>
        <li class="active">Creación de una Empresa</li>
    @stop

    @section('content')
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {!! Form::open(['method'=> 'POST', 'id'=> 'form', 'action'=> 'Businesses\BusinessController@store', 'files'=> true,
                        'onsubmit'=> 'return false']) !!}
            <div id="wizard">
                <h1>Datos de la Empresa</h1>

                <div>
                    <div class="form-group">
                        {!! Form::label('business_name', 'Nombre de la Empresa') !!}
                        {!! Form::text('business_name', NULL, ['class'=> 'form-control required',
                                                               'placeholder'=> 'Ejemplo: Plumrose Latinoamericana C.A.']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('business_rif', 'Rif de la Empresa') !!}
                        {!! Form::text('business_rif', NULL, ['class'=> 'form-control required',
                                                               'placeholder'=> 'Ejemplo: J-25625165-1']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('business_logo', 'Logo de la Empresa') !!}
                        {!! Form::file('business_logo') !!}
                        <p class="help-block">En caso de no poseer ningún logo, será asignado uno por defecto. (Máx: 600kb)</p>
                    </div>
                    <div class="form-group">
                        {!! Form::label('business_description', 'Descripción de la Empresa') !!}
                        {!! Form::textarea('business_description', NULL, ['class'=> 'form-control required',
                                                               'placeholder'=> 'Ejemplo: Empresa encargada de la producción, fabricación y empaque de embutidos',
                                                               'rows'=> 3]) !!}
                    </div>
                </div>

                <h1>Sucursales</h1>
                <div>
                    <div class="form-group">
                        {!! Form::label('headquarters_advice', 'Por los momentos no deseo registrar una Sucursal') !!}
                        {!! Form::checkbox('headquarters_advice', true, true, ['id'=> 'headquarters_advice', 'activated'=> 'true']) !!}
                    </div>
                    <div id="step_content">
                        Si es marcada esta opción, será creada la entidad empresarial sin ninguna entidad física asociada a ella.
                        Las mismas podrán ser agregadas posteriormente desde el listado de Empresas
                    </div>
                    <div class="form-group" id="headquarter_form" style="display:none">
                        <div class="form-group">
                            {!! Form::label('headquarter_name', 'Nombre de la Sucursal') !!}
                            {!! Form::text('headquarter_name', NULL, ['class'=> 'form-control required',
                                                                   'placeholder'=> 'Ejemplo: Plumrose Latinoamericana - Sede Cagua']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('headquarter_description', 'Descripción de la Sucursal') !!}
                            {!! Form::textarea('headquarter_description', NULL, ['class'=> 'form-control required',
                                                                   'placeholder'=> 'Ejemplo: Empresa encargada de la producción, fabricación y empaque de embutidos',
                                                                   'rows'=> 3]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('headquarter_direction', 'Dirección de la Sucursal') !!}
                            {!! Form::textarea('headquarter_direction', NULL, ['class'=> 'form-control required',
                                                                   'placeholder'=> 'Ejemplo: Av. Fuerzas Armadas, Edif. Libertador',
                                                                   'rows'=> 3]) !!}
                        </div>
                    </div>
                </div>
                <!--Hidden fields for counters-->
                <input type="hidden" id="headquarters_counter" name="headquarters_counter" value="0">
            </div>
        {!! Form::close() !!}
    @stop

    @section('post_script')
        {!! Html::script('js/jquery.steps.js') !!}
        {!! Html::script('js/localization/messages_es.js') !!}
        <script>
            $(document).ready(function() {
                var form= $('#form');

                var wizard= $('#wizard').steps({
                    labels: {
                        cancel: 'Cancelar',
                        current: 'Actual',
                        next: 'Siguiente',
                        previous: 'Anterior',
                        loading: 'Cargando',
                        finish: 'Finalizar',
                    },
                    transitionEffect: "slideLeft",
                    stepsOrientation: 1 ,

                    onStepChanging: function(event, currentIndex, newIndex) {
                        // Allways allow previous action even if the current form is not valid!
                        if (currentIndex > newIndex) {
                            return true;
                        }
                        // Needed in some cases if the user went back (clean up)
                        if (currentIndex < newIndex)
                        {
                            // To remove error styles
                            form.find(".body:eq(" + newIndex + ") label.error").remove();
                            form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                        }
                        form.validate().settings.ignore = ":disabled,:hidden";
                        return form.valid();
                    },
                    onFinished: function (event, currentIndex)
                    {
                        form.removeAttr('onsubmit');
                        form.submit();
                    }
                });

                $('input[type="checkbox"]').on('change', function(){
                    $('#step_content').slideToggle();
                    $('#headquarter_form').slideToggle();
                });
            });
        </script>
    @stop