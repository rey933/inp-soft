<?php

use Illuminate\Database\Seeder;

class InpsaselPlacesTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inpsasel-places-type')->insert([
            'title'=> 'ZONAS INDUSTRIALES',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-places-type')->insert([
            'title'=> 'OBRAS, CONSTRUCCIÓN, CANTERA, MINA A CIELO ABIERTO',
            'inpsasel-code'=> 2,
        ]);

        DB::table('inpsasel-places-type')->insert([
            'title'=> 'LUGARES AGRICOLAS, DE CRIA DE ANIMALES, DE PISCICULTURA, ZONA FORESTAL',
            'inpsasel-code'=> 3,
        ]);

        DB::table('inpsasel-places-type')->insert([
            'title'=> 'LUGARES DE ACTIVIDAD TERCIARIA, OFICINAS, ÁREAS DE OCIO, VARIOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-places-type')->insert([
            'title'=> 'CENTROS SANITARIOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-places-type')->insert([
            'title'=> 'LUGARES PUBLICOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-places-type')->insert([
            'title'=> 'DOMICILIOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-places-type')->insert([
            'title'=> 'LUGARES DE ACTIVIDADES DEPORTIVAS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-places-type')->insert([
            'title'=> 'EN EL AIRE, ELEVADOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-places-type')->insert([
            'title'=> 'SUBTERRÁNEOS',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-places-type')->insert([
            'title'=> 'EN EL AGUA',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-places-type')->insert([
            'title'=> 'EN MEDIO HIPERBARICO',
            'inpsasel-code'=> 1,
        ]);

        DB::table('inpsasel-places-type')->insert([
            'title'=> 'OTROS TIPOS DE LUGAR',
            'inpsasel-code'=> 1,
        ]);
    }
}
