<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/user', function (Request $request) {
//    return $request->user();
//})->middleware('auth:api');

Route::group(['prefix'=> 'user'], function() {
    Route::get('search/{id}', 'Users\UserController@getUser');
    Route::get('remove/{id}', 'Users\UserController@removeUser');
});

Route::group(['prefix'=> 'businesses'], function() {
    Route::get('all', 'Businesses\BusinessController@all');
});

Route::group(['prefix'=> 'employees'], function() {
    Route::get('search/{identity}', 'Employees\EmployeeController@search');
});

Route::group(['prefix'=> 'headquarters'], function() {
    Route::get('search-by-business/{business_id}', 'Headquarters\HeadquarterController@searchByBusiness');
});

Route::group(['prefix'=> 'blocks'], function() {
	Route::get('all', 'Headquarters\BlocksController@all');
	Route::get('search/{headquarter_id}', 'Headquarters\BlocksController@search');
    Route::post('store/{headquarter_id}', 'Headquarters\BlocksController@store');
    Route::get('delete/{block_id}', 'Headquarters\BlocksController@delete');
});

Route::group(['prefix'=> 'departments'], function() {
	Route::get('search/{block_id}', 'Headquarters\DepartmentsController@search');
	Route::post('store/{block_id}', 'Headquarters\DepartmentsController@store');
	Route::get('delete/{department_id}', 'Headquarters\DepartmentsController@delete');
});

Route::group(['prefix'=> 'areas'], function() {
	Route::get('all', 'Headquarters\AreasController@all');
    Route::get('search/{department_id}', 'Headquarters\AreasController@searchByDepartment');
    Route::post('store/', 'Headquarters\AreasController@store');
    Route::get('attach/{department_id}/{area_id}', 'Headquarters\AreasController@attachArea');
    Route::get('delete/{area_id}', 'Headquarters\AreasController@delete');
    Route::get('detach/{department_id}/{area_id}', 'Headquarters\AreasController@detachArea');
});

Route::group(['prefix'=> 'places'], function() {
	Route::get('all', 'Headquarters\AreasController@all');
    Route::get('search/{area_id}', 'Headquarters\PlacesController@search');
});

Route::group(['prefix'=> 'reports'], function() {
    Route::group(['prefix'=> 'plans'], function() {
        Route::get('show/{plan_id}', 'Reports\PlansController@show');
        Route::post('store/', 'Reports\PlansController@store');
    });

    Route::group(['prefix'=> 'tasks'], function() {
        Route::get('all', 'Reports\TasksController@index');
        Route::get('show/{task_id}', 'Reports\TasksController@show');
        Route::post('store/', 'Reports\TasksController@store');
        Route::get('search/{plan_id}', 'Reports\PlansController@searchTasks'); //Search all Task in an Action Plan
        Route::get('delete/{task_id}', 'Reports\TasksController@destroy');
        Route::get('change-status/{task_id}/{new_status}', 'Reports\TasksController@updateStatus');
    });
});

Route::group(['prefix'=> 'findings'], function() {
    Route::post('statistics', 'Reports\FindingsController@statistics');
});