<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Administrador de Bloques</h4>
      </div>
      <div class="modal-body">
          <table class="table table-striped">
              <thead>
                <th>Nombre</th>
                <th>Desciprción</th>
                <th>Acciones</th>
              </thead>
              <tbody>
                @foreach($blocks as $block)
                    <tr data-id="{!! $block->id !!}">
                        <td>{!! $block->name !!}</td>
                        <td>{!! $block->description !!}</td>
                        <td>
                            <button class="btn btn-danger btn-sm delete-btn"
                                    title="Eliminar Bloque"
                                    value="blocks">
                                <i class="fa fa-times"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
              </tbody>
          </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar Administrador</button>
      </div>
    </div>
  </div>
</div>