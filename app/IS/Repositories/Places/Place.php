<?php

namespace App\IS\Repositories\Places;

use App\IS\Repositories\Base\BaseModel;

class Place extends BaseModel
{
    protected $table= 'places';
    protected $fillable= [
    	'area_id', 'name',
    ];

    
}